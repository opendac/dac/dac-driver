Getting started
===============

Installing required tools
-------------------------

Basically it is required to install Git, CMake and some C/C++ compiler.

.. tabs::

    .. tab:: Microsoft Windows

        Download and install:

        #. `Git <https://git-scm.com/downloads>`_
        #. `CMake <https://cmake.org/download>`_
        #. `Microsoft Visual Studio <https://visualstudio.microsoft.com>`_ (make sure to select "Desktop development with C/C++")

        If you are planning to build `Python <https://www.python.org>`_ or `Matlab <https://www.mathworks.com/>`_ interfaces, then you also need to install them.

    .. tab:: Debian based distros

        Run the following command:

        .. code::

            sudo apt install build-essential cmake git python3-dev


Obtaining the driver
--------------------

You have basically two options here:

#. Download a `zip file <https://gitlab.com/opendac/dac/dac-driver/-/archive/main/dac-driver-main.zip>`_ with the repository and then extract it, or
#. Clone the `repository <https://gitlab.com/opendac/dac/dac-driver.git>`_ using git


Build the driver
----------------

Use you favorite file manager and navigate to the folder with the repository files. Run the CMake to configure the
development tools:

1. Open CMake GUI and click on "Browse Source..." button to select the folder with the repository
2. Click on "Browse Build..." button and select a folder where all the generated binaries will stay
3. Now, hit "Configure" button and wait

.. note::

    If you want to build Python, Matlab or any other interface, it is required to enable it on the CMake. It may be
    required to set Python or Matlab location.

When finished, hit "Generate" button. After it finishes, the project will be configured at build folder. If you are
using Microsoft Visual Studio, then the solution file is available in such folder and you may start to develop or build
your interfaces.




