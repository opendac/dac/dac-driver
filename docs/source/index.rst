.. DAC Driver documentation master file, created by
   sphinx-quickstart on Fri Jul 14 14:32:42 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Open-DAC Driver's documentation!
===========================================

What is this?
-------------

Open-DAC is a project aiming to empower people (most students and hobbyists) with a simple, easy to use and extremely low cost tools for implementing projects relating control systems. It is very simple:

- you buy a blue pill (and maybe a, ST-LINK debugger) with less than 10 bucks
- you flash the firmware into the microcontroller
- you start studying/developing control systems projects using C++, Python or Simulink (without fancy toolboxes)


Why another development framework?
----------------------------------

Because it does not focus on the hardware. Imagine the following: you are studying Model Predictive Control or any other fancy control approach which requires a lot of calculation to be done. Unfortunately, the Arduino will probably do not provide the required processing power that your project requires. And even if it does, you will probably want to use some matrix library or solver, for example. And it will probably need to be ported to the microcontroller architecture. You will probably face this issue even with another microcontroller, such as ARM.

So, instead of focusing in the remote board, we focus in the computer. We make the computer send instructions to the remote board to execute them. For example, read encoder, turn the LED on, etc. And all the calculation is done in your computer, with friendly tools.


What can I do with it?
------------------------

- read up to 3x incremental encoders (count or time measurement)
- read 4x analog signals with voltages from 0 to 3.3 V (blue pill has an A/D of 12 bits)
- generate 4x PWM channels
- use 3x GPIO digital output (push-pull and open-drain modes)
- read 3x GPIO digital input
- (in the future) connect I2C devices/sensors
- (in the future) connect OneWire devices/sensors


What do I need to begin?
--------------------------

- a blue pill (STM32F103C8T6, caution with chinese clones)
- a st-link (it is possible to load the firmware with a simple usb to serial converter)


Contents
--------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started/index
   board/index
   api/index
   examples


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
