API Reference
=============

In this project some constants were defined. It doesn't matter if you are using C++ or Python. They will have the same name and content.

.. note::

    If you are using C++ and have some doubt about what constant to use, check the function (or method) signature . It will help you to decide.

Time functions
--------------

The following functions are available for usage:

.. doxygenfunction:: getTimeMs
   :project: DacDriver

.. doxygenfunction:: getTimeUs
   :project: DacDriver

.. doxygenfunction:: delayMs
   :project: DacDriver

.. doxygenfunction:: delayUs
   :project: DacDriver

.. caution::

    Generating delays in PC is tricky.
    It is possible to use C++ ``std::this_thread::sleep_for`` to make delays. However, the resulting delay precision may not
    be adequate for control purposes. The idea of ``sleep_for is``: put the thread to sleep for x ms. The problem is: your
    operational system scheduler may take longer the x ms to call the thread back. How long? Most GNU/Linux systems use
    1 ms for their scheduler. Microsoft Windows
    (`apparently <https://randomascii.wordpress.com/2020/10/04/windows-timer-resolution-the-great-rule-change>`_)
    changed their scheduler to 10 ms. This means that, if you
    require 2 ms delay, calling ``std::this_thread::sleep_for(2ms)`` would implement a delay of (at least) 10 ms (at
    Microsoft Windows). The OpenDAC API automatically changes your scheduler frequency to the best possible, however,
    depending on your system load, this may not be enough.

    The ``delayMs`` and ``delayUs`` functions will block for the specified time. These functions have the purpose of providing the most
    precise delay possible. This means they will prevent their thread to be put to sleep and the direct consequence is
    high cpu usage.

    The best you can do is: if you require long delays, then stick to ``std::this_thread::sleep_for``. If you require more
    precision, then use ``delayUs``.

Channels
--------

Almost all peripherals of the board are channels. For example: the board has 4 PWM channels.

.. doxygenenum:: Channel
    :project: DacDriver


Logic levels
------------

.. doxygenenum:: PinState
    :project: DacDriver


Encoder modes
-------------

.. doxygenenum:: EncoderMode
    :project: DacDriver


Encoder resolution
------------------

.. doxygenenum:: EncoderResolution
    :project: DacDriver


Encoder direction
-----------------

.. doxygenenum:: EncoderDirection
    :project: DacDriver



Class definitions
-----------------

.. toctree::

    memory_table
    dac_class

