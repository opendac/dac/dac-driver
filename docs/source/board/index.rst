Board information
=================

Pinout
------

.. note::

    The following pins are 5V tolerant: PB3, PB4, PB6, PB7, PB8, PB9, PB10, PB11, PB12, PB13, PB14, PB15, PA8, PA9, PA10 and PA15

.. caution::

    The following pins are NOT 5V tolerant: PA0, PA1, PA2, PA3, PA4, PA5, PA6, PA7, PB0, PB1, PB5, PC13, PC14 and PC15

.. figure:: pinout.png
      :align: center



