#include "dac.hpp"

/*
 * In this example we will be reading analog voltages.
 */

constexpr bool disableAutoSync = false;
Dac* board;

void myFunction()
{
    /*
     * Just print the analog voltage.
     */
    printf("Adc0 = %.4fV\n", board->analogRead(CH1));
}

int main()
{
    /*
     * Please notice that we disabled the autoSync. This will force us to call sync() after every command to the board.
     */

#if WIN32
    board = new Dac("COM5");
#else
    board = new Dac("/dev/ttyACM0", disableAutoSync);
#endif

    if (board->isConnected())
    {
        // Tell the daq to send data every 100 ms, i.e., 10 Hz sampling frequency.
        board->enableAutoRead(10, myFunction);
        board->sync();

        // This program will run during 10 seconds
        std::this_thread::sleep_for(std::chrono::seconds (10));

        // Disable the autoread
        board->disableAutoRead();
        board->sync();
    }

    printf("And... we are done\n");

    delete board;

    return 0;
}

