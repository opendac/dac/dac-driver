#include <iostream>
#include "dac.hpp"

Dac* board;

int main()
{
    uint64_t ti, tf, dt, total_time = 0, average_time, n_eval = 0;
    ti = getTimeUs();

#if WIN32
    board = new Dac("COM10");
#else
    board = new Dac("/dev/ttyACM0");
#endif

    tf = getTimeUs();
    dt = tf - ti;
    std::cout << "Initialization took " << dt << " us" << std::endl;

    if (board->isConnected())
    {
        std::cout << "Board was initialized correctly" << std::endl;
        board->gpioConfig(CH3, GpioModeOutput);

        int i;
        for (i = 0; i < 20; i++)
        {
            ti = getTimeUs();                                 // Mark time before running gpioWrite
            board->gpioWrite(CH3, HIGH);      // Make a transaction with the board
            tf = getTimeUs();                                 // Mark time after running gpioWrite
            dt = tf - ti;                                     // Calculate how much time was required
            total_time = total_time + dt;
            n_eval = n_eval + 1;
            std::cout << "communication took " << dt << " us" << std::endl;

            /*
             * This is a low resolution kind of delay. OpenDAC takes measures to increase your computer scheduler
             * resolution. However, it may be a good idea to check your resulting resolution.
             *
             * If you *really* need high precision delays, then you may be use our delayMs or delayUs. However, both of
             * these functions will block your CPU, making it goes to 100% usage.
             */
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            ti = getTimeUs();
            board->gpioWrite(CH3, LOW);
            tf = getTimeUs();
            dt = tf - ti;
            total_time = total_time + dt;
            n_eval = n_eval + 1;
            std::cout << "communication took " << dt << " us (" << i + 1 << "/20)" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        // And delete the pointer
        delete board;

        average_time = total_time / n_eval;

        printf("--- Report ---\n");
        printf("Number of commands: %d\n", int(n_eval));
        printf("Average time per command: %d us\n", int(average_time));
        printf("--------------\n");

    }
    else
    {
        std::cout << "(error) Board was NOT initialized correctly" << std::endl;
    }

    return 0;
}
