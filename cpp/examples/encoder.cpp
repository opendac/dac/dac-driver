#include <iostream>
#include "dac.hpp"

/*
 * In this example we will be reading pulse count and speed from a single incremental encoder. Again, you will not use
 * the Z cable.
 *
 * The connections to be done are:
 * - encoder data 0 cable (it may be CH1, CHA, whatever) goes to pins PA8 and PA15
 * - encoder data 1 cable (it may be CH2, CHB, whatever) goes to pins PA9 and PB3
 *
 * We will use two encoder interfaces: encoder 1 (to read pulses count) and 2 to read speed, i.e., the time between
 * the pulses.
 */

constexpr bool disableAutoSync = false;
Dac* board;

void myFunction()
{
    /*
     * Just read the encoders.
     */
    printf("Count = %d and speed = %d\n", board->encoderRead(1), board->encoderRead(2));
}

int main()
{
#if WIN32
    board = new Dac("COM5");
#else
    board = new Dac("/dev/ttyACM0", disableAutoSync);

    /*
     * Please notice that we disabled the autoSync. This will force us to call sync() after every command to the board.
     */
#endif

    if (board->isConnected())
    {
        /*
         * Configure encoder 1 to count pulses.
         */
        board->encoderSetMode(1, PulseCount);
        board->encoderSetDirection(1, CW);        // The other option would be CCW
        board->encoderSetResolution(1, HighResolution);
        board->encoderSetFilter(1, 15);
        board->encoderReset(1);

        /*
         * Configure encoder 2 to count pulses.
         */
        board->encoderSetMode(2, TimeMeasurement);
        board->encoderSetDirection(2, CW);        // The other option would be CCW
        board->encoderSetResolution(2, HighResolution);
        board->encoderSetFilter(2, 15);
        board->sync();

        // Tell the daq to send data every 100 ms
        board->enableAutoRead(0.1, myFunction);
        board->sync();

        // This program will run during 10 seconds
        delayMs(10000U);

        // Disable the autoread
        board->disableAutoRead();
        board->sync();
    }

    printf("And... we are done\n");

    delete board;

    return 0;
}
