#include <iostream>
#include <cmath>
#include "dac.hpp"

Dac* board;
constexpr double pwmFrequencyHz = 400;
constexpr double escMinPulseLengthUs = 0;
constexpr double escStopPulseLengthUs = 1000;
constexpr double escMaxPulseLengthUs = 2000;

static double escLengthToDuty(double length_us)
{
    // limit to esc1MinPulseLengthUs <= length_us <= esc1MaxPulseLengthUs
    length_us = fmax(length_us, escMinPulseLengthUs);
    length_us = fmin(length_us, escMaxPulseLengthUs);
    return pwmFrequencyHz * (length_us / 1e6f);
}

static bool areEqual(const double n1, const double n2, const double tolerance=0.001)
{
    return std::abs(n1 - n2) < tolerance;
}

int main()
{
#if WIN32
    board = new Dac("COM5");
#else
    board = new DacUsb("/dev/ttyACM0");
#endif

    if (board->isConnected())
    {
        std::cout << "Board was initialized correctly" << std::endl;

        board->pwmSetFrequency(400);
        double stopDuty = escLengthToDuty(escStopPulseLengthUs);

        // Is the ESC initialized?
//        bool escInitialized = areEqual(board->pwmGetDuty(CH1), stopDuty);
//        if (!escInitialized)
//        {
//            printf("Initializing the ESC ...\n");
//            board->pwmSetDuty(CH1, escLengthToDuty(escMaxPulseLengthUs));
//            board->sync();
//            delayMs(2000);
//            board->pwmSetDuty(CH1, escLengthToDuty(escMinPulseLengthUs));
//            board->sync();
//            delayMs(4000);
//            board->pwmSetDuty(CH1, escLengthToDuty(escStopPulseLengthUs));
//            board->sync();
//            printf("The ESC is initialized\n");
//        }
//        else
//        {
//            printf("The ESC was already initialized\n");
//        }


//        board->gpioWrite(CH1, HIGH);
//        board->pwmSetDuty(CH1, escLengthToDuty(escMaxPulseLengthUs));
//        delayMs(2000);
//        board->pwmSetDuty(CH1, escLengthToDuty(escMinPulseLengthUs));
//        delayMs(4000);
//        board->pwmSetDuty(CH1, escLengthToDuty(escStopPulseLengthUs));

        board->pwmSetDuty(CH1, escLengthToDuty(escMaxPulseLengthUs));
        board->gpioWrite(CH1, HIGH);
        delayMs(2000);
        board->pwmSetDuty(CH1, escLengthToDuty(escMinPulseLengthUs));
        delayMs(2000);

        // run the experiment
        board->pwmSetDuty(CH1, escLengthToDuty(1100));
        delayMs(4000);
        board->pwmSetDuty(CH1, stopDuty);
        delayMs(1000);

        // turn it off
        board->gpioWrite(CH1, LOW);
    }
    else
    {
        std::cout << "(error) Board was NOT initialized correctly" << std::endl;
    }

    return 0;
}
