#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>
#include "dac.hpp"

constexpr unsigned int ExperimentTotalTimeSecs = 10;
constexpr unsigned int ExperimentSampleFreqHz = 10;
Dac* board;
uint64_t timeLastEvent = 0;
std::vector<double> sampleTimes;

void myFunction()
{
    /*
     * This function (defined by the user) will behave just like an interrupt.
     * It will run automatically when the board sends data to the computer.
     *
     * It is worth noting that the board will be controlling the sample time.
     * Let's see how good is the sample time imposed by the board.
     *
     * Note: if you are using the board through the usb, then it may be a good
     *       idea not to be using mouse, keyboard and other usb devices during the
     *       control task, because they will consume usb bandwidth.
     */
    const uint64_t timeNow = getTimeUs();
    const double elapsedTimeMs = double(timeNow - timeLastEvent) / 1000.0;
    if (timeLastEvent != 0)
    {
        printf("Time since last call: %.2f ms\n", elapsedTimeMs);
    }
    timeLastEvent = timeNow;

    /*
     * You can also run commands.
     */
    if (board->getGpioOutputState(CH3) == HIGH)
    {
        board->gpioWrite(CH3, LOW);
    }
    else
    {
        board->gpioWrite(CH3, HIGH);
    }

    // Store sample time
    sampleTimes.push_back(elapsedTimeMs);
}

int main()
{
    timeLastEvent = getTimeUs();

#if WIN32
    board = new Dac("COM10");
#else
    board = new Dac("/dev/ttyACM0");
#endif

    if (board->isConnected())
    {
        // Set the LED pin as output
        board->gpioConfig(CH3, GpioModeOutput);

        // Tell the daq to send data every 100 ms, i.e., 10 Hz
        board->enableAutoRead(ExperimentSampleFreqHz, myFunction);

        // This program will run during 10 seconds
        std::this_thread::sleep_for(std::chrono::seconds (ExperimentTotalTimeSecs));

        // And now the board will not blink
        board->disableAutoRead();
        printf("Disabling the auto read\n");
        std::this_thread::sleep_for(std::chrono::milliseconds (100));
        if (board->isAutoReadEnabled())
            std::cout << "Auto read is ENABLED" << std::endl;
        else
            std::cout << "Auto read is disabled" << std::endl;
    }

    // Ignore the first measurement
    sampleTimes.erase(sampleTimes.begin());

    // Calculate mean and standard deviation
    double sum = std::accumulate(sampleTimes.begin(), sampleTimes.end(), 0.0);
    double mean = sum / static_cast<double>(sampleTimes.size());
    double sqSum = std::inner_product(sampleTimes.begin(), sampleTimes.end(), sampleTimes.begin(), 0.0);
    double stdev = std::sqrt(sqSum / static_cast<double>(sampleTimes.size()) - mean * mean);

    printf("--- Report ---");
    printf("Mean(sample times) = %f.4 ms\n", mean);
    printf("Standard deviation(sample times) = %f.4 ms\n", stdev);

    return 0;
}
