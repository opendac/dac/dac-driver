#include "dac.hpp"

DacNrf *board;

int main()
{
    uint64_t ti, tf, elapsedTime;
    int i, totalTime = 0;

#if WIN32
    board = new DacNrf("COM4");
#else
    board = new DacNrf("/dev/ttyACM0");
#endif

    ti = getTimeUs();
    board->connect();
    printf("Elapsed time = %d us\n", int(getTimeUs() - ti));

    if (board->isConnected())
    {
        std::cout << "Board is connected! :-)" << std::endl;

        for (i = 0; i < 50; i++)
        {
            ti = getTimeUs();
            board->digitalWrite(CH3, HIGH);
            tf = getTimeUs();
            elapsedTime = tf - ti;
            totalTime = totalTime + int(elapsedTime);
            printf("Elapsed time = %d us\n", int(elapsedTime));
            delayMs(100);

            ti = getTimeUs();
            board->digitalWrite(CH3, LOW);
            tf = getTimeUs();
            elapsedTime = tf - ti;
            printf("Elapsed time = %d us\n", int(elapsedTime));
            totalTime = totalTime + int(elapsedTime);
            delayMs(100);
        }

        printf("--- Report ---\n");
        printf("Average time: %d us\n", totalTime / 100);
    }
}


