#include <iostream>
#include "dac.hpp"

Dac* board;

int main()
{
#if WIN32
    board = new Dac("COM5");
#else
    board = new Dac("/dev/ttyACM0");
#endif

    board->pwmSetFrequency(12345);
    double frequency = board->pwmGetFrequency();
    std::cout << "Frequency: " << frequency << " Hz\n";

    // Set 25% duty cycle on pwm channel 1
    board->pwmSetDuty(CH1, 0.25);

    // Set dead time to 5 us
    board->pwmSetDeadTime(5000);

    // Apply duty cycle of 50 % on pwm channel 1
    board->pwmSetDuty(CH1, 0.5);

    delete board;
}
