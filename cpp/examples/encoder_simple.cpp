#include <iostream>
#include "dac.hpp"

/*
 * In this example we will be reading pulse count from an incremental encoder.
 *
 * Because we are using encoder 1, you will need to connect your incremental encoder data cables to pins PA8
 * and PA9. Do not connect the Z cable. These pins are 5V tolerant, so do not worry if your encoder is 5V.
 *
 * You also do not need to worry about swapping encoder cables. You can do it by software if the direction is wrong.
 */

constexpr bool disableAutoSync = false;
Dac* board;

void myFunction()
{
    /*
     * Just read the encoder.
     */
    printf("Encoder = %d\n", board->encoderRead(1));
}

int main()
{
    /*
     * Please notice that we disabled the autoSync. This will force us to call sync() after every command to the board.
     */

#if WIN32
    board = new Dac("COM5");
#else
    board = new Dac("/dev/ttyACM0", disableAutoSync);
#endif

    if (board->isConnected())
    {
        /*
         * Configure encoder 1 to count pulses.
         */
        board->encoderConfig(1, PulseCount, HighResolution, CW, 15);
        board->encoderReset(1);

        /*
         * Before we continue. Take a look on all the above configurations. If autoSync was enabled, then each line
         * would cause a communication transaction with the board. The events would be:
         * 1. the board receives the instruction "set the encoder mode", which costs 125 us of communication
         * 2. the board would do the work
         * 3. the board answers "Ok, I finished the job", costing more 125 us
         * 4. the computer then sends "set the encoder direction", costing more 125 us
         *
         * Each command transaction costs at least 250 us to be executed. This is because of the USB.
         *
         * However, because we disabled the autoSync, all the configuration will be done in a single transaction. Of
         * course, the board will take more time to process the instructions, but in the end, it will be faster.
         *
         * This is particularly useful when writing to multiple PWM channels.
         */
        board->sync();

        // Tell the daq to send data every 100 ms
        board->enableAutoRead(0.1, myFunction);
        board->sync();

        // This program will run during 10 seconds
        std::this_thread::sleep_for(std::chrono::seconds (10));

        // Disable the autoread
        board->disableAutoRead();
        board->sync();
    }

    printf("And... we are done\n");

    delete board;

    return 0;
}
