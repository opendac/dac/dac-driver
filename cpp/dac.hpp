#ifndef DAC_DRIVER_HPP
#define DAC_DRIVER_HPP

// specific libraries for Windows systems
#ifdef WIN32
#include <SDKDDKVer.h>
#endif

#include <iostream>
#include <functional>
#include <chrono>
#include <memory>
#include <thread>
#include "opendac_definitions.hpp"

#ifdef WITH_NRFUSB
#include "cpp/nrfusb.hpp"
#endif

constexpr uint8_t DAC_FIRMWARE_MIN_VERSION = 2;

// Forward declaration of the DacImpl struct and its functions
struct DacImpl;
std::shared_ptr<DacImpl> dacInit(const std::string& boardAddress, bool autoSync, unsigned int timeoutMs, unsigned int maxRetries);
bool   dacIsConnected(const std::shared_ptr<DacImpl>& self);
void   dacReadAll(const std::shared_ptr<DacImpl>& self);
void   dacReadMeasurements(const std::shared_ptr<DacImpl>& self);
void   dacSync(const std::shared_ptr<DacImpl>& self);
void   dacGpioConfig(const std::shared_ptr<DacImpl>& self, Channel channel, GpioMode pinMode);
void   dacGpioWrite(const std::shared_ptr<DacImpl>& self, Channel channel, int state);
int    dacGpioRead(const std::shared_ptr<DacImpl>& self, Channel channel);
double dacAnalogRead(const std::shared_ptr<DacImpl>& self, Channel channel);
void   dacPwmSetDeadTime(const std::shared_ptr<DacImpl>& self, double deadTimeNs);
void   dacPwmConfig(const std::shared_ptr<DacImpl>& self, Channel channel, uint8_t mode, bool fastMode, bool chPolarity, bool chnPolarity, bool chIdleState, bool chnIdleState);
void   dacPwmSetDuty(const std::shared_ptr<DacImpl>& self, Channel channel, double duty);
double dacPwmGetDuty(const std::shared_ptr<DacImpl>& self, Channel channel);
void   dacPwmSetFrequency(const std::shared_ptr<DacImpl>& self, double freqInHz);
double dacPwmGetFrequency(const std::shared_ptr<DacImpl>& self);
int    dacEncoderRead(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber);
void   dacEncoderConfig(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber, EncoderMode mode, EncoderResolution resolution, EncoderDirection direction, unsigned int clockCycles);
void   dacEncoderReset(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber);
void   dacEnableAutoRead(const std::shared_ptr<DacImpl>& self, double periodSecs, std::function<void(void)> function);
void   dacDisableAutoRead(const std::shared_ptr<DacImpl>& self);
int    dacIsAutoReadEnabled(const std::shared_ptr<DacImpl>& self);
int    dacGetFirmwareVersion(const std::shared_ptr<DacImpl>& self);
int    dacGetBoardId(const std::shared_ptr<DacImpl>& self);
void   dacDestructor(const std::shared_ptr<DacImpl>& self);

/**
 * Get current time (in milliseconds) since system initialization.
 *
 * @return current time in milliseconds.
 */
uint64_t getTimeMs();

/**
 * Get current time (in microseconds) since system initialization.
 *
 * @return current time in microseconds.
 */
uint64_t getTimeUs();

/**
 * Blocks the CPU during time_to_wait milliseconds.
 *
 * @param time_to_wait
 */
void delayMs(uint64_t time_to_wait);

/**
 * Blocks the CPU during time_to_wait microseconds.
 *
 * @param time_to_wait
 */
void delayUs(uint64_t time_to_wait);

class Dac {
public:
    /**
     * @brief This is the constructor for the USB flavor of the Dac object.
     *
     * @param boardAddress the serial port name. In Linux systems it will be "/dev/ttyACM0" or something like that. In
     *                     Windows systems it will be something like "COM5".
     * @param AutoSync if true, then the writing commands to the board will sync automatically. If not, the user needs
     *                 to call sync() after writing operations. Its purpose is to avoid unnecessary transactions with
     *                 the board. Default: true.
     *
     * Example with two communication transactions:
     * @code
     * dac = Dac("COM5");
     * dac.gpioWrite(CH1, HIGH);
     * dac.gpioWrite(CH3, HIGH);
     * dac.close();
     * @endcode
     *
     * Example with only one communication transaction:
     * @code
     * dac = Dac("COM5", setAutoSync=false);
     * dac.gpioWrite(CH1, HIGH);  // Only updates local memory table.
     * dac.gpioWrite(CH3, HIGH);  // Only updates local memory table.
     * dac.sync();                   // Data will be sent to the board here.
     * dac.close();
     * @endcode
     */
    explicit Dac(const std::string& boardAddress, bool AutoSync=true, unsigned int timeoutMs=3, unsigned int maxRetries=2)
    {
        self = dacInit(boardAddress, AutoSync, timeoutMs, maxRetries);
    }

    /**
     * Destructor.
     */
    ~Dac()
    {
        dacDestructor(self);
    }

    /**
     * @brief Verifies if communication with the board is established.
     * @return true if the board is connected or false otherwise.
     */
    bool isConnected()
    {
        return dacIsConnected(self);
    }

    /**
     * @brief It reads all the memory of the board. This command should not be used for reading the measurements.
     *
     * @note For that purpose Dac::readAll or Dac::enableAutoRead.
     */
    void readAll()
    {
        dacReadAll(self);
    }

    /**
     * @brief It reads all the measurements made by the board.
     *
     * @note If you want a precise sample time, then you should take a look at Dac::enableAutoRead.
     */
    void readMeasurements()
    {
        dacReadMeasurements(self);
    }

    /**
     * @brief Forces to write data to the board. It is useful to configure several peripherals with only one communication
     * transaction. This function DOES NOT read data from the board.
     */
    void sync()
    {
        dacSync(self);
    }

    /**
     * @brief This method is used to configure some gpio channel.
     * @param channel the GPIO channel. Example: CH3.
     * @param pinMode the pin operation mode.
     */
    void gpioConfig(Channel channel, GpioMode pinMode)
    {
        dacGpioConfig(self, channel, pinMode);
    }

    /**
     * @brief This method writes HIGH or LOW value to an output digital pin.
     * @param channel the GPIO channel. Example: CH3.
     * @param state HIGH or LOW.
     */
    void gpioWrite(Channel channel, int state)
    {
        dacGpioWrite(self, channel, state);
    }

    /**
     * @brief Reads the logic state of a digital input pin.
     * @param channel the INPUT GPIO channel. Example: CH1.
     * @return HIGH or LOW.
     */
    int gpioRead(Channel channel)
    {
        return dacGpioRead(self, channel);
    }

    /**
     * @brief Reads the analog value at some analog channel.
     * @param channel the ANALOG channel. Example: CH4.
     * @return the voltage (in V) on the analog pin.
     */
    double analogRead(Channel channel)
    {
        return dacAnalogRead(self, channel);
    }

    /**
     * @brief This method is used to set the PWM dead time of all channels.
     * @param deadTimeNs the new dead time value in nanoseconds.
     */
    void pwmSetDeadTime(double deadTimeNs)
    {
        dacPwmSetDeadTime(self, deadTimeNs);
    }

    /**
     * Configures a PWM channel.
     * @param channel the PWM channel. Example: CH2.
     * @param mode the PWM mode. It can be 1 or 2.
     * @param fastMode enable fast mode?
     * @param chPolarity the polarity of PWM CH output. It can be HIGH or LOW.
     * @param chnPolarity the polarity of PWM CHN output. It can be HIGH or LOW.
     * @param chIdleState the idle state of PWM CH output. It can be HIGH or LOW.
     * @param chnIdleState the idle state of PWM CHN output. It can be HIGH or LOW.
     */
    void pwmConfig(Channel channel, uint8_t mode, bool fastMode, bool chPolarity, bool chnPolarity, bool chIdleState, bool chnIdleState)
    {
        dacPwmConfig(self, channel, mode, fastMode, chPolarity, chnPolarity, chIdleState, chnIdleState);
    }

    /**
     * @brief This method sets the duty cycle of some PWM channel.
     * @param channel the PWM channel. Example: CH3.
     * @param duty a real number between 0 and 1.
     */
    void pwmSetDuty(Channel channel, double duty)
    {
        dacPwmSetDuty(self, channel, duty);
    }

    /**
     * @brief Returns the duty cycle at some PWM channel.
     * @param channel the PWM channel. Example: CH4.
     * @return the duty cycle at the PWM channel.
     */
    double pwmGetDuty(Channel channel)
    {
        return dacPwmGetDuty(self, channel);
    }

    /**
     * @brief This method sets the PWM frequency.
     * @param freqInHz the frequency (in Hz) of the PWM signal.
     */
    void pwmSetFrequency(double freqInHz)
    {
        dacPwmSetFrequency(self, freqInHz);
    }

    /**
     * @brief Returns the actual PWM frequency in Hz.
     * @return a real number representing the PWM frequency.
     */
    double pwmGetFrequency()
    {
        return dacPwmGetFrequency(self);
    }

    /**
     * @brief This method reads the encoder.
     * @param encoderNumber is the encoder number. It can 1, 2 or 3.
     * @return if the encoder is in pulse count mode, then it will return the encoder pulse count value. If the encoder
     *         is in time measurement mode, then it will return time in micro-seconds.
     */
    int encoderRead(unsigned int encoderNumber)
    {
        return dacEncoderRead(self, encoderNumber);
    }

    /**
     * @brief Sets the encoder mode.
     * @param encoderNumber is the encoder number. It can 1, 2 or 3.
     * @param mode it can be encoderModePulseCount or encoderModeTimeMeasurement.
     * @param resolution it can be LowResolution or HighResolution.
     * @param positiveDirection it can be CW or CCW.
     * @param clockCycles it is the number of cpu cycles that the logic state of the channel must be constant to be
     *                    recognized as stable. It is a number between (including) 0 and 15.
     * @warning Pulse count is completely implemented by hardware, i.e., it does not cost any CPU time in the
     *          microcontroller. However, the time measurement is implemented with interrupts. So, it will cost CPU
     *          time and then the input signal should be limited to several kHz.
     */
    void encoderConfig(unsigned int encoderNumber, EncoderMode mode, EncoderResolution resolution, EncoderDirection direction, unsigned int clockCycles)
    {
        dacEncoderConfig(self, encoderNumber, mode, resolution, direction, clockCycles);
    }

    /**
     * @brief This method resets the encoder count value.
     * @param encoderNumber is the encoder number. It can 1, 2 or 3.
     */
    void encoderReset(unsigned int encoderNumber)
    {
        dacEncoderReset(self, encoderNumber);
    }

    /**
     * @brief It enables the auto read feature. It will make the board to send all measurements periodically.
     *
     * @param freqHz is the frequency (in Hertz) between the updates. This value must range from 0.123 to 2000 Hz.
     *        It is possible to reach up to 8 kHz through USB, but control tasks usually require one read and one write
     *        operation. So, the sampling rate will drop to 4 kHz. It is even worse if you enable write confirmation,
     *        what will force board to send an packet signalizing transmission success, what will take more 125 us from
     *        USB and leading frequency to 2.666 khz. Another issue is related to another devices connected on USB. As
     *        you connect more and more devices (mouse, keyboard, webcam, etc), they will use 125 us each transmission.
     *        So, if you really need to use the maximum sample rate, then do not move mouse, press keys, etc, during the
     *        control task.
     * @param function is a function defined by the user that will be called when new data is read from the
     *        microcontroller. This function signature must be void function(void).
     *
     * @note The period has resolution of 1 millisecond.
     */
    void enableAutoRead(double freqHz, std::function<void(void)> function=nullptr)
    {
        dacEnableAutoRead(self, freqHz, function);
    }

    /**
     * @brief It disables the auto read feature.
     */
    void disableAutoRead()
    {
        dacDisableAutoRead(self);
    }

    /**
     * @brief Returns the state of autoRead.
     * @return true if it is enabled and false otherwise.
     */
    bool isAutoReadEnabled()
    {
        return dacIsAutoReadEnabled(self);
    }

    /**
     * @brief This method reads the firmware version.
     * @return an integer representing the version.
     */
    unsigned int getFirmwareVersion()
    {
        return dacGetFirmwareVersion(self);
    }

    /**
     * @brief This method reads the board id value.
     * @return it is expected to read the value 29.
     */
    unsigned int getBoardId()
    {
        return dacGetBoardId(self);
    }

private:
    std::shared_ptr<DacImpl> self;
};

#ifdef WITH_NRFUSB

class DacNrf : public Dac {
public:
    /**
     * @brief This is the constructor for the USB flavor of the Dac object.
     *
     * @param portName the serial port name. In Linux systems it will be "/dev/ttyACM0" or somthing like that. In
     *                 Windows systems it will be somthing like "COM5".
     * @param setAutoSync if true, then the writing commands to the board will sync automatically. If not, the user
     *                    needs to call sync() after writing operations. Its purpose is to avoid unnecessary
     *                    transactions with the board. Default: true.
     *
     * Example with two communication transactions:
     * @code
     * dac = DacNrf("COM5");
     * dac.digitalWrite(CH1, HIGH);
     * dac.digitalWrite(CH3, HIGH);
     * dac.close();
     * @endcode
     *
     * Example with only one communication transaction:
     * @code
     * dac = DacUsb("COM5", setAutoSync=false);
     * dac.digitalWrite(CH1, HIGH);  // Only updates local memory table.
     * dac.gpioWrite(CH3, HIGH);  // Only updates local memory table.
     * dac.sync();                   // Data will be sent to the board here.
     * dac.close();
     * @endcode
     */
    DacNrf(const std::string& portName, bool setAutoSync=true);

    /**
     * The destructor.
     */
    ~DacNrf();

    void connect();

    void setChannel(int channel);
    void setPowerLevel(rf24_pa_dbm_e level);
    void programChannel(int channel);
    void programPowerLevel(rf24_pa_dbm_e level);

protected:
    Nrfusb *radio;
    bool isRadioConnected = false;

    /**
     * The implementation of the virtual implRead method to the serial port. It does not block the CPU.
     */
    void implRead() override;

    /**
     * The implementation of the virtual implWrite method to the serial port. It will block the CPU.
     * @param buffer is a pointer to data that should be sent.
     * @param size is how many bytes should be sent.
     */
    void implWrite(uint8_t* buffer, uint8_t size) override;

    void callbackWhatHappened(bool tx_ok, bool tx_fail, bool rx_ready);

    bool dynamicPayloadSize = false;
};

#endif

#endif //DAC_DRIVER_HPP
