#ifndef DEFINITIONS_HPP
#define DEFINITIONS_HPP

#include <cstdint>

/**
 * It defines all the possible channels available in the board. It does not necessarily means that the board has 4
 * channels of a peripheral. For example, the Blue Pill was configured to have 3 Gpio output channels but 4 Pwm
 * channels.
 */
typedef enum Channel {
    CH1 = 1,   ///< Channel 1.
    CH2 = 2,   ///< Channel 2.
    CH3 = 3,   ///< Channel 3.
    CH4 = 4,   ///< Channel 4.
    CH5 = 5,   ///< Channel 5.
    CH6 = 6,   ///< Channel 6.
    CH7 = 7,   ///< Channel 7.
} Channel;

constexpr int LOW = 0;    ///< It means a low level.
constexpr int HIGH = 1;   ///< It means a high level. Not necessarily +5 V or +3.3 V.

/**
 * The possible pin modes.
 */
typedef enum GpioMode {
    GpioModeInput = 0,     ///< Pin operates in input mode (without any pull resistor).
    GpioModeOutput,        ///< Pin operates in output mode (push pull).
    GpioModeNrfCe,         ///< Not supported.
    GpioModeNrfCs,         ///< Not supported.
    GpioModeNrfIrq,        ///< Not supported.
    GpioModeEthernetIrq,   ///< Not supported.
} eGpioMode;

/**
 * There are two possible encoder modes.
 */
typedef enum EncoderMode {
    /**
     * In this mode the encoder interface will count pulses. This is done entirely by hardware.
     */
    PulseCount = 0,

    /**
     * In this mode the encoder interface will measure time between edges.
     * This is implemented with interrupts and although much effort was put in the code, it will support only a few
     * of kHz.
     */
    TimeMeasurement = 1,
} EncoderMode;

/**
 * Each encoder can be configured with one of the following resolutions. Depending of the operating mode, it may or not
 * increase the CPU usage.
 */
typedef enum EncoderResolution {
    /**
     * In pulse count mode, this option implements the X2 quadrature, i.e., capture all edges of CHA
     * (it uses CHB only to decide CW or CCW direction).
     *
     * In time measurement mode, this option implements X1 quadrature, i.e., measure the time between two consecutive
     * rising edges on CHA. CHB is used only for deciding between CW or CCW.
     */
    LowResolution = 0,

    /**
     * In pulse count mode, this option implements the X4 quadrature, i.e., capture all edges of CHA and CHB.
     *
     * In time measurement mode, this option implements X2 quadrature, i.e., measure the time between rising and falling
     * edges on CHA. CHB is used only for deciding between CW or CCW.
     */
    HighResolution = 1,
} EncoderResolution;

typedef enum EncoderDirection {
    /**
     * Encoder will increment +1 if running in counter-clockwise.
     */
    CCW = 0,

    /**
     * Encoder will increment +1 if running in clockwise.
     */
    CW = 1,
} EncoderDirection;

#endif //DEFINITIONS_HPP
