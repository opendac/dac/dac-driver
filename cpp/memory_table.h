#ifndef DAC_FIRMWARE_MEMORY_TABLE_H
#define DAC_FIRMWARE_MEMORY_TABLE_H

#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

typedef struct {
    uint8_t ch1       : 1;
    uint8_t ch2       : 1;
    uint8_t ch3       : 1;
    uint8_t ch4       : 1;
    uint8_t ch5       : 1;
    uint8_t ch6       : 1;
    uint8_t ch7       : 1;
    uint8_t reserved1 : 1;
    uint8_t enc1Sign  : 1;
    uint8_t enc2Sign  : 1;
    uint8_t enc3Sign  : 1;
    uint8_t reserved2 : 1;
    uint8_t reserved3 : 4;
} GpioInputState;

typedef struct {
    uint8_t dataRate           : 2;
    uint8_t paLevel            : 2;
    uint8_t autoAck            : 1;
    uint8_t dynamicPayloadSize : 1;
    uint8_t reserved           : 2;
} RadioConfig;

typedef struct {
    uint8_t mode         : 1;
    uint8_t fastMode     : 1;
    uint8_t chPolarity   : 1;
    uint8_t chnPolarity  : 1;
    uint8_t chIdleState  : 1;
    uint8_t chnIdleState : 1;
    uint8_t reserved     : 2;
} PwmConfig;

typedef struct {
    uint8_t filter     : 4;
    uint8_t reserved   : 1;
    uint8_t direction  : 1;
    uint8_t resolution : 1;
    uint8_t mode       : 1;
} EncoderConfig;

typedef struct {
    uint8_t ch1 : 1;
    uint8_t ch2 : 1;
    uint8_t ch3 : 1;
    uint8_t ch4 : 1;
    uint8_t ch5 : 3;
    uint8_t ch6 : 3;
    uint8_t ch7 : 3;
} GpioConf;

typedef struct {
    uint8_t ch1 : 1;
    uint8_t ch2 : 1;
    uint8_t ch3 : 1;
    uint8_t ch4 : 1;
    uint8_t ch5 : 1;
    uint8_t ch6 : 1;
    uint8_t ch7 : 1;
} GpioOutputState;

typedef struct {
    uint8_t        boardId;             // Address 0
    uint8_t        version;             // Address 1
    uint8_t        ethernetMac[6];      // Addresses 2, 3, 4, 5, 6 and 7
    // alignment
    uint8_t        ethernetIp[4];       // Addresses 8, 9, 10 and 11
    uint8_t        ethernetMask[4];     // Addresses 12, 13, 14 and 15
    uint8_t        ethernetGw[4];       // Addresses 16, 17, 18 and 19
    // alignment
    uint8_t        radioOwnAddress[5];  // Addresses 20, 21, 22, 23 and 24
    uint8_t        radioChannel;        // Address 25
    uint8_t        radioPcAddress[5];   // Addresses 26, 27, 28, 29 and 30
    RadioConfig    radioConfig;         // Address 31
    // alignment
    uint16_t       pwmPrescaler;        // Addresses 32 and 33
    uint16_t       pwmMaxCount;         // Addresses 34 and 35
    // alignment
    uint8_t        pwmDeadTime;         // Address 36
    PwmConfig      pwm1Config;          // Address 37
    PwmConfig      pwm2Config;          // Address 38
    PwmConfig      pwm3Config;          // Address 39
    // alignment
    EncoderConfig  enc1Config;          // Address 40
    EncoderConfig  enc2Config;          // Address 41
    EncoderConfig  enc3Config;          // Address 42
    uint8_t        uartConfig;          // Address 43
    // alignment
    uint16_t       autoReadPeriod;      // Addresses 44 and 45
    GpioConf       gpioConfig;          // Addresses 46 and 47
    // alignment
    GpioInputState gpioInputState;      // Addresses 48 and 49
    uint16_t       enc1Data;            // Addresses 50 and 51
    // alignment
    uint16_t       enc2Data;            // Addresses 52 and 53
    uint16_t       enc3Data;            // Addresses 54 and 55
    uint16_t       adc1Data;            // Addresses 56 and 57
    uint16_t       adc2Data;            // Addresses 58 and 59
    uint16_t       adc3Data;            // Addresses 60 and 61
    uint16_t       adc4Data;            // Addresses 62 and 63
    uint16_t       pwm1DutyCycle;       // Addresses 64 and 65
    uint16_t       pwm2DutyCycle;       // Addresses 66 and 67
    uint16_t       pwm3DutyCycle;       // Addresses 68 and 69
    uint16_t       dac1Data;            // Addresses 70 and 71
    uint16_t       dac2Data;            // Addresses 72 and 73
    uint16_t       dac3Data;            // Addresses 74 and 75
    // alignment
    uint16_t       dac4Data;            // Addresses 76 and 77
    GpioOutputState gpioOutputState;    // Address 78
} MemoryTable;

#define TABLE_ADDRESS_OF_BOARD_ID           0
#define TABLE_ADDRESS_OF_VERSION            1
#define TABLE_ADDRESS_OF_ETHERNET_MAC       2
#define TABLE_ADDRESS_OF_ETHERNET_IP        8
#define TABLE_ADDRESS_OF_ETHERNET_MASK      12
#define TABLE_ADDRESS_OF_ETHERNET_GW        16
#define TABLE_ADDRESS_OF_RADIO_OWN_ADDRESS  20
#define TABLE_ADDRESS_OF_RADIO_CHANNEL      25
#define TABLE_ADDRESS_OF_RADIO_PC_ADDRESS   26
#define TABLE_ADDRESS_OF_RADIO_CONFIG       31
#define TABLE_ADDRESS_OF_PWM_PRESCALER      32
#define TABLE_ADDRESS_OF_PWM_MAX_COUNT      34
#define TABLE_ADDRESS_OF_PWM_DEADTIME       36
#define TABLE_ADDRESS_OF_PWM1_CONFIG        37
#define TABLE_ADDRESS_OF_PWM2_CONFIG        38
#define TABLE_ADDRESS_OF_PWM3_CONFIG        39
#define TABLE_ADDRESS_OF_ENC1_CONFIG        40
#define TABLE_ADDRESS_OF_ENC2_CONFIG        41
#define TABLE_ADDRESS_OF_ENC3_CONFIG        42
#define TABLE_ADDRESS_OF_UART_CONFIG        43
#define TABLE_ADDRESS_OF_AUTOREAD_PERIOD    44
#define TABLE_ADDRESS_OF_GPIO_CONFIG        46
#define TABLE_ADDRESS_OF_GPIO_INPUT_STATE   48
#define TABLE_ADDRESS_OF_ENC1_DATA          50
#define TABLE_ADDRESS_OF_ENC2_DATA          52
#define TABLE_ADDRESS_OF_ENC3_DATA          54
#define TABLE_ADDRESS_OF_ADC1_DATA          56
#define TABLE_ADDRESS_OF_ADC2_DATA          58
#define TABLE_ADDRESS_OF_ADC3_DATA          60
#define TABLE_ADDRESS_OF_ADC4_DATA          62
#define TABLE_ADDRESS_OF_PWM1_DUTY_CYCLE    64
#define TABLE_ADDRESS_OF_PWM2_DUTY_CYCLE    66
#define TABLE_ADDRESS_OF_PWM3_DUTY_CYCLE    68
#define TABLE_ADDRESS_OF_DAC1_DATA          70
#define TABLE_ADDRESS_OF_DAC2_DATA          72
#define TABLE_ADDRESS_OF_DAC3_DATA          74
#define TABLE_ADDRESS_OF_DAC4_DATA          76
#define TABLE_ADDRESS_OF_GPIO_OUTPUT_STATE  78

#endif //DAC_FIRMWARE_MEMORY_TABLE_H
