#ifdef WIN32
/*
 * Asio library requires some Windows definitions available at this file.
 */
#include <SDKDDKVer.h>

/*
 * The following library adds support to __nop instruction.
 * https://stackoverflow.com/questions/54918884/implementations-for-asmnop-in-windows
 */
#include <intrin.h>
#endif

/*
 * Include the asio library and build it as static library. More information can be found at:
 * https://think-async.com/Asio/asio-1.18.0/doc/asio/using.html
 */
#include <asio.hpp>
#include <asio/impl/src.hpp>

#include <algorithm>
#include <functional>
#include <memory>
#include <vector>
#include <mutex>
#include <thread>
#include <utility>
#include <string>

#include <chrono>
using namespace std::chrono;

#include <system_error>
#include "protocol.h"
#include "receiver.h"
#include "opendac_definitions.hpp"
#include "memory_table.h"

#ifdef WIN32
#include <windows.h>
#include <winternl.h>
extern "C" NTSYSAPI NTSTATUS NTAPI NtSetTimerResolution(ULONG DesiredResolution, BOOLEAN SetResolution, PULONG CurrentResolution);
#endif

constexpr int BUFFER_SIZE  = 256;                           ///< Size (in bytes) of the buffer.
constexpr double K_ADC = 3.3f / 4095.0f;                    ///< The A/D gain.
constexpr double PWM_MIN_FREQUENCY =			0.02f;      ///< The PWM min frequency. Used only for sanity check.
constexpr double PWM_MAX_FREQUENCY =			720000.0f;  ///< The PWM max frequency. Used only for sanity check.
constexpr double PWM_MIN_DUTY =					0;          ///< The PWM min duty cycle. Used only for sanity check.
constexpr double PWM_MAX_DUTY =					1;          ///< The PWM max duty cycle. Used only for sanity check.

/**
 * A very useful macro. It will interpret the content 'x' as uint8_t.
 */
#define u8(x) static_cast<uint8_t>(x)

uint64_t getTimeMs()
{
    const uint64_t now = duration_cast<milliseconds>(time_point_cast<milliseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return now;
}

uint64_t getTimeUs()
{
    const uint64_t now = duration_cast<microseconds>(time_point_cast<microseconds>(high_resolution_clock::now()).time_since_epoch()).count();
    return now;
}

void delayMs(uint64_t time_to_wait)
{
    const uint64_t deadline = 1000UL * time_to_wait + getTimeUs();
    while (deadline > getTimeUs())
    {
#ifdef WIN32
        __nop();
#else
        asm("nop");
#endif
    }
}

void delayUs(uint64_t time_to_wait)
{
    const uint64_t deadline = time_to_wait + getTimeUs();
    while (deadline > getTimeUs())
    {
#ifdef WIN32
        __nop();
#else
        asm("nop");
#endif
    }
}

constexpr uint8_t registerSize[256] = {
    1, // Board ID
    1, // Version
    6, // Ethernet MAC address
    6,
    6,
    6,
    6,
    6,
    4, // Ethernet IP
    4,
    4,
    4,
    4, // Ethernet subnet mask
    4,
    4,
    4,
    4, // Ethernet gateway
    4,
    4,
    4,
    5, // Radio own address
    5,
    5,
    5,
    5,
    1, // Radio channel
    5, // Radio pc address
    5,
    5,
    5,
    5,
    1, // Radio config
    2, // PWM prescaler
    2,
    2, // PWM max count
    2,
    4, // PWM dead time
    4, // PWM1 config
    4, // PWM2 config
    4, // PWM3 config
    1, // Encoder 1 config
    1, // Encoder 2 config
    1, // Encoder 3 config
    1, // UART config
    2, // Auto read period in ms
    2,
    2, // gpio config
    2,
    2, // gpio input state
    2,
    2, // Encoder 1 data
    2,
    2, // Encoder 2 data
    2,
    2, // Encoder 3 data
    2,
    2, // ADC1 data
    2,
    2, // ADC2 data
    2,
    2, // ADC3 data
    2,
    2, // ADC4 data
    2,
    2, // PWM1 duty cycle
    2,
    2, // PWM2 duty cycle
    2,
    2, // PWM3 duty cycle
    2,
    2, // DAC1 data
    2,
    2, // DAC2 data
    2,
    2, // DAC3 data
    2,
    2, // DAC4 data
    2,
    1, // gpio output state
};

struct DacImpl {
    asio::io_context*  asioContext;                   ///< The asio io context.
    asio::serial_port* serialPort;                    ///< The serial port object.
    std::mutex* mutex;
    std::thread threadAsio;                           ///< This thread executes asio io_context.run().
    MemoryTable table;                                ///< The memory table. The idea is to keep this table synced with the board.
    uint8_t rxBuffer[256];                            ///< The rx buffer. A place to store received data.
    commMsg instructionPacket;                        ///< An instance to a instruction packet.
    commReceiver receiver;                            ///< The receiver is a state machine capable of receiving messages from the board.
    bool waitingRx;                                   ///< An auxiliary variable to the asynchronous communication.
    bool waitingTx;                                   ///< An auxiliary variable to the asynchronous communication.
    bool connected;                                   ///< By default it is assumed the board is connected. However this will be checked later.
    bool autoSync;                                    ///< If true, any command will communicate immediately with the board.
    unsigned int timeoutMs;                           ///< How long the driver will wait for a response from board. It may change in different implementations.
    unsigned int maxRetries;                          ///< How many tries the driver will try to send instruction packets to the board.
    std::function<void(void)> callbackUser;           ///< The callback specified by the user when measurements are received.
    std::vector<uint8_t> syncList;                    ///< A list of register that should updated in the next communication transaction.
    uint64_t t0;

#ifdef WIN32
    ULONG oldResolution;
#endif
};

static void prvCalculateTimerFrequency(const double newFreqHz, uint16_t* maxCount, uint16_t* prescaler)
{
    /*
     * The frequency is calculated with:
     * ftimer = SystemCoreClock / ((actual_top + 1) * (actual_divider + 1))
     */
    const double max_top = 0xFFFF, max_divider = 0xFFFF;
    const auto ftimer = 72e6;
    const double fmin = ftimer / ((max_top + 1) * (max_divider + 1));
    int32_t top, divider;

    if (newFreqHz > ftimer)
    {
        divider = 0;
        top = 100;
    }
    else if (newFreqHz <= fmin)
    {
        divider = static_cast<int32_t>(max_divider);
        top = static_cast<int32_t>(max_top);
    }
    else
    {
        // a modest first guess
        divider = static_cast<int32_t>(ftimer / ((max_top + 1) * newFreqHz) - 1);
        top     = static_cast<int32_t>(ftimer / (static_cast<double>(divider + 1) * newFreqHz) - 1);

        while (top > static_cast<int32_t>(max_top))
        {
            divider = divider + 1;
            top     = static_cast<int32_t>(ftimer / (static_cast<double>(divider + 1) * newFreqHz) - 1);
        }
    }

    *maxCount = static_cast<uint16_t>(top);
    *prescaler = static_cast<uint16_t>(divider);
}

void prvRead(const std::shared_ptr<DacImpl>& self);
void prvWrite(const std::shared_ptr<DacImpl>& self, uint8_t* buffer, uint8_t size);

void prvDecodeReadAnswer(const std::shared_ptr<DacImpl>& self)
{
    auto tableData = reinterpret_cast<uint8_t*>(&self->table);
    uint8_t* parameters = commMsgGetAddrOfParameters(&self->receiver.packet);
    uint16_t firstRegister = commMsgGetParameter(&self->receiver.packet, 1);
    uint16_t size = commMsgGetParameter(&self->receiver.packet, 2);
    memcpy(&tableData[firstRegister], &parameters[3], size);
}

void prvDecodeStatusPacket(const std::shared_ptr<DacImpl>& self)
{
    switch (commMsgGetField(&self->receiver.packet, commMsgFieldType)) {
        case u8(commMsgTypeAnsWrite):
        case u8(commMsgTypeAnsMultiWrite):
            self->waitingTx = false;
            break;

        case u8(commMsgTypeAnsRead):
            prvDecodeReadAnswer(self);
            self->waitingRx = false;
            break;

        case u8(commMsgTypeAnsAutoRead):
            prvDecodeReadAnswer(self);
            if (self->callbackUser)
            {
                std::thread (self->callbackUser).detach();
            }
            break;

        default:
            break;
    }

    // Clean the statusPacket to avoid problems
    memset(&self->receiver.packet.buffer, 0, commMsgMaxSize);
}

void prvReadDataFromBoard(const std::shared_ptr<DacImpl>& self, uint8_t firstRegister, uint8_t howMany)
{
    // auxiliary variables
    unsigned int retries = self->maxRetries;
    uint64_t deadlineUs;
    uint8_t size;

    // build a new instruction packet
    commMsgCreateEmptyMsg(&self->instructionPacket, commMsgTypeRead);
    commMsgAppendParameter(&self->instructionPacket, firstRegister);
    commMsgAppendParameter(&self->instructionPacket, howMany);
    commMsgUpdateChecksumValue(&self->instructionPacket);

    // get the instruction packet information
    size = commMsgGetTotalPacketSize(&self->instructionPacket);

    // send the data and wait for the response
    self->waitingRx = true;
    while ((retries > 0) && (self->waitingRx))
    {
        // request data from the board
        prvWrite(self, self->instructionPacket.buffer, size);

        // calculate the deadline
        deadlineUs = getTimeUs() + 1000 * self->timeoutMs;

        // now let's wait something to happen
        while ((deadlineUs > getTimeUs()) && (self->waitingRx)) {}

        retries = retries - 1;
    }
}

void prvAddToSyncList(const std::shared_ptr<DacImpl>& self, uint8_t address)
{
    std::vector<uint8_t>::iterator it;
    it = find(self->syncList.begin(), self->syncList.end(), address);

    // if the element is not in the list
    if (it == self->syncList.end())
    {
        self->syncList.push_back(address);
    }
}

void prvCallbackFinishedReceiving(const std::shared_ptr<DacImpl>& self, const std::error_code& ec, size_t bytesTransferred)
{
    // Prevent warnings about unused variable ec
    (void) ec;

    // Auxiliary variables
    size_t i;

    // If it is not a canceled operation
    if (bytesTransferred > 0)
    {
        // Process the received bytes
        for (i = 0; i < bytesTransferred; i++)
        {
            commReceiverProcess(&self->receiver, self->rxBuffer[i]);

            if (commReceiverHasNewMessage(&self->receiver))
            {
                prvDecodeStatusPacket(self);
                commReceiverClean(&self->receiver);
            }
        }
    }

    // Trigger another async read before exiting to make sure that asio_context.run() is still running
    prvRead(self);
}

void prvCallbackFinishedSending(const std::shared_ptr<DacImpl>& self, const std::error_code& ec, size_t bytesTransferred)
{
    // Prevent warnings about unused variable ec
    (void) self;
    (void) ec;
    (void) bytesTransferred;
}

void prvRead(const std::shared_ptr<DacImpl>& self)
{
    asio::async_read(
        *self->serialPort,
        asio::buffer(self->rxBuffer, BUFFER_SIZE),
        asio::transfer_at_least(1),
        std::bind(
                prvCallbackFinishedReceiving,
                self,
                std::placeholders::_1,  // asio::placeholders::error_code
                std::placeholders::_2   // asio::placeholders::bytes_transferred
        )
    );
}

void prvWrite(const std::shared_ptr<DacImpl>& self, uint8_t* buffer, uint8_t size)
{
    asio::async_write(
        *self->serialPort,
        asio::buffer(buffer, size),
        asio::transfer_exactly(size),
        std::bind(
                prvCallbackFinishedSending,
                self,
                std::placeholders::_1,  // asio::placeholders::error_code
                std::placeholders::_2   // asio::placeholders::bytes_transferred
        )
    );
}

void prvThreadRun(const std::shared_ptr<DacImpl>& self)
{
    while ( self->connected )
    {
        // It may happen that the context is empty, and it requires to be restarted
        // so let's schedule an async read to make sure it does not stop again
        prvRead(self);

        // Run the context
        self->asioContext->run();
        self->asioContext->restart();
    }
}

bool dacIsConnected(const std::shared_ptr<DacImpl>& self)
{
    return self->connected;
}

void dacReadAll(const std::shared_ptr<DacImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);
    prvReadDataFromBoard(self, 0, 80);
}

void dacReadMeasurements(const std::shared_ptr<DacImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);
    prvReadDataFromBoard(self, 48, 32);
}

void dacSync(const std::shared_ptr<DacImpl>& self)
{
    uint8_t address, size;
    size_t i;
    unsigned int retries = self->maxRetries;
    uint64_t deadlineUs;
    auto tableData = reinterpret_cast<uint8_t*>(&self->table);

    if (!self->syncList.empty())
    {
        if (self->syncList.size() == 1)
        {
            commMsgCreateEmptyMsg(&self->instructionPacket, commMsgTypeWrite);
        }
        else
        {
            commMsgCreateEmptyMsg(&self->instructionPacket, commMsgTypeMultiWrite);
        }

        for (i = 0; i < self->syncList.size(); i++)
        {
            address = self->syncList[i];
            commMsgAppendParameter(&self->instructionPacket, address);
            commMsgAppendParameter(&self->instructionPacket, registerSize[address]);
            commMsgAppendParameters(&self->instructionPacket, &tableData[address], registerSize[address]);
        }

        self->syncList.clear();

        commMsgUpdateChecksumValue(&self->instructionPacket);
        size = commMsgGetTotalPacketSize(&self->instructionPacket);

        self->waitingTx = true;
        while ((retries > 0) && (self->waitingTx))
        {
            // Send data
            prvWrite(self, self->instructionPacket.buffer, size);

            // Wait for the response
            deadlineUs = getTimeUs() + 1000 * self->timeoutMs;
            while ((deadlineUs > getTimeUs()) && (self->waitingTx)) {}

            retries = retries - 1;
        }
    }
}

void dacGpioConfig(const std::shared_ptr<DacImpl>& self, Channel channel, GpioMode pinMode)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    switch (channel) {
        case CH1:
            if (pinMode == GpioModeInput || pinMode == GpioModeOutput)
                self->table.gpioConfig.ch1 = static_cast<uint8_t>(pinMode);
            break;
        case CH2:
            if (pinMode == GpioModeInput || pinMode == GpioModeOutput)
                self->table.gpioConfig.ch2 = static_cast<uint8_t>(pinMode);
            break;
        case CH3:
            if (pinMode == GpioModeInput || pinMode == GpioModeOutput)
                self->table.gpioConfig.ch3 = static_cast<uint8_t>(pinMode);
            break;
        case CH4:
            if (pinMode == GpioModeInput || pinMode == GpioModeOutput)
                self->table.gpioConfig.ch4 = static_cast<uint8_t>(pinMode);
            break;
        case CH5:
            self->table.gpioConfig.ch5 = static_cast<uint8_t>(pinMode);
            break;
        case CH6:
            self->table.gpioConfig.ch6 = static_cast<uint8_t>(pinMode);
            break;
        case CH7:
            self->table.gpioConfig.ch7 = static_cast<uint8_t>(pinMode);
            break;
    }

    prvAddToSyncList(self, TABLE_ADDRESS_OF_GPIO_CONFIG);
    if (self->autoSync) dacSync(self);
}

void dacGpioWrite(const std::shared_ptr<DacImpl>& self, Channel channel, int state)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    const auto value = static_cast<uint8_t>(state);
    switch (channel) {
        case CH1:
            self->table.gpioOutputState.ch1 = value;
            break;
        case CH2:
            self->table.gpioOutputState.ch2 = value;
            break;
        case CH3:
            self->table.gpioOutputState.ch3 = value;
            break;
        case CH4:
            self->table.gpioOutputState.ch4 = value;
            break;
        case CH5:
            self->table.gpioOutputState.ch5 = value;
            break;
        case CH6:
            self->table.gpioOutputState.ch6 = value;
            break;
        case CH7:
            self->table.gpioOutputState.ch7 = value;
            break;
        default:
            break;
    }

    prvAddToSyncList(self, TABLE_ADDRESS_OF_GPIO_OUTPUT_STATE);
    if (self->autoSync) dacSync(self);
}

int dacGpioRead(const std::shared_ptr<DacImpl>& self, Channel channel)
{
    int value = -1;
    switch (channel) {
        case CH1:
            value = self->table.gpioInputState.ch1;
            break;
        case CH2:
            value = self->table.gpioInputState.ch2;
            break;
        case CH3:
            value = self->table.gpioInputState.ch3;
            break;
        case CH4:
            value = self->table.gpioInputState.ch4;
            break;
        case CH5:
            value = self->table.gpioInputState.ch5;
            break;
        case CH6:
            value = self->table.gpioInputState.ch6;
            break;
        case CH7:
            value = self->table.gpioInputState.ch7;
            break;
        default:
            break;
    }

    return value;
}

double dacAnalogRead(const std::shared_ptr<DacImpl>& self, Channel channel)
{
    double value = -1.0f;
    switch (channel) {
        case CH1:
            value = K_ADC * static_cast<double>(self->table.adc1Data);
            break;
        case CH2:
            value = K_ADC * static_cast<double>(self->table.adc2Data);
            break;
        case CH3:
            value = K_ADC * static_cast<double>(self->table.adc3Data);
            break;
        case CH4:
            value = K_ADC * static_cast<double>(self->table.adc4Data);
            break;
        default:
            break;
    }

    return value;
}

void dacPwmConfig(const std::shared_ptr<DacImpl>& self, Channel channel, uint8_t mode, bool fastMode, bool chPolarity, bool chnPolarity, bool chIdleState, bool chnIdleState)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    switch (channel) {
        case CH1:
            self->table.pwm1Config.mode = mode;
            self->table.pwm1Config.fastMode = static_cast<uint8_t>(fastMode);
            self->table.pwm1Config.chPolarity = static_cast<uint8_t>(chPolarity);
            self->table.pwm1Config.chnPolarity = static_cast<uint8_t>(chnPolarity);
            self->table.pwm1Config.chIdleState = static_cast<uint8_t>(chIdleState);
            self->table.pwm1Config.chnIdleState = static_cast<uint8_t>(chnIdleState);
            break;
        case CH2:
            self->table.pwm2Config.mode = mode;
            self->table.pwm2Config.fastMode = static_cast<uint8_t>(fastMode);
            self->table.pwm2Config.chPolarity = static_cast<uint8_t>(chPolarity);
            self->table.pwm2Config.chnPolarity = static_cast<uint8_t>(chnPolarity);
            self->table.pwm2Config.chIdleState = static_cast<uint8_t>(chIdleState);
            self->table.pwm2Config.chnIdleState = static_cast<uint8_t>(chnIdleState);
            break;
        case CH3:
            self->table.pwm3Config.mode = mode;
            self->table.pwm3Config.fastMode = static_cast<uint8_t>(fastMode);
            self->table.pwm3Config.chPolarity = static_cast<uint8_t>(chPolarity);
            self->table.pwm3Config.chnPolarity = static_cast<uint8_t>(chnPolarity);
            self->table.pwm3Config.chIdleState = static_cast<uint8_t>(chIdleState);
            self->table.pwm3Config.chnIdleState = static_cast<uint8_t>(chnIdleState);
            break;
        default:
            break;
    }

    prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM_DEADTIME);
    if (self->autoSync) dacSync(self);
}

void dacPwmSetDeadTime(const std::shared_ptr<DacImpl>& self, double deadTimeNs)
{
    constexpr double tdtsNs = (1e3 / 72);
    constexpr double maxDeadTimeCase1Ns =             127 * tdtsNs;  // Case 1: DTG[7:5] = 0XX
    constexpr double maxDeadTimeCase2Ns = (64 + 63) *   2 * tdtsNs;  // Case 2: DTG[7:5] = 10X
    constexpr double maxDeadTimeCase3Ns = (32 + 31) *   8 * tdtsNs;  // Case 3: DTG[7:5] = 110
    constexpr double maxDeadTimeCase4Ns = (32 + 31) *  16 * tdtsNs;  // Case 4: DTG[7:5] = 111

    if (deadTimeNs < maxDeadTimeCase1Ns)
        self->table.pwmDeadTime = static_cast<uint8_t>(deadTimeNs / tdtsNs);
    else if (deadTimeNs <= maxDeadTimeCase2Ns)
        self->table.pwmDeadTime = (static_cast<uint8_t>(deadTimeNs / (2 * tdtsNs)) - 64) | (1 << 7);
    else if (deadTimeNs <= maxDeadTimeCase3Ns)
        self->table.pwmDeadTime = (static_cast<uint8_t>(deadTimeNs / (8 * tdtsNs)) - 32) | (3 << 6);
    else if (deadTimeNs <= maxDeadTimeCase4Ns)
        self->table.pwmDeadTime = (static_cast<uint8_t>(deadTimeNs / (16 * tdtsNs)) - 32) | (7 << 5);
    else
        self->table.pwmDeadTime = 255;  // Apply the maximum dead time possible

    prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM_DEADTIME);
    if (self->autoSync) dacSync(self);
}

void dacPwmSetDuty(const std::shared_ptr<DacImpl>& self, Channel channel, double duty)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);
    auto maxCount = static_cast<double>(self->table.pwmMaxCount);

    if ((duty >= PWM_MIN_DUTY) && (duty <= PWM_MAX_DUTY))
    {
        switch (channel) {
            case CH1:
                self->table.pwm1DutyCycle = static_cast<uint16_t>(duty * maxCount);
                prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM1_DUTY_CYCLE);
                break;
            case CH2:
                self->table.pwm2DutyCycle = static_cast<uint16_t>(duty * maxCount);
                prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM2_DUTY_CYCLE);
                break;
            case CH3:
                self->table.pwm3DutyCycle = static_cast<uint16_t>(duty * maxCount);
                prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM3_DUTY_CYCLE);
                break;
            default:
                break;
        }

        if (self->autoSync) dacSync(self);
    }
}

double dacPwmGetDuty(const std::shared_ptr<DacImpl>& self, Channel channel)
{
    double duty = -1.0f;
    auto maxCount = static_cast<double>(self->table.pwmMaxCount);

    switch (channel) {
        case CH1:
            duty = static_cast<double>(self->table.pwm1DutyCycle) / maxCount;
            break;
        case CH2:
            duty = static_cast<double>(self->table.pwm2DutyCycle) / maxCount;
            break;
        case CH3:
            duty = static_cast<double>(self->table.pwm3DutyCycle) / maxCount;
            break;
        default:
            break;
    }

    return duty;
}

void dacPwmSetFrequency(const std::shared_ptr<DacImpl>& self, double freqInHz)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    uint16_t prescalerRaw, maxCountRaw;
    freqInHz = std::min(freqInHz, PWM_MAX_FREQUENCY);
    freqInHz = std::max(freqInHz, PWM_MIN_FREQUENCY);
    prvCalculateTimerFrequency(freqInHz, &maxCountRaw, &prescalerRaw);
    self->table.pwmPrescaler = prescalerRaw;
    self->table.pwmMaxCount = maxCountRaw;
    prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM_PRESCALER);
    prvAddToSyncList(self, TABLE_ADDRESS_OF_PWM_MAX_COUNT);

    if (self->autoSync) dacSync(self);
}

double dacPwmGetFrequency(const std::shared_ptr<DacImpl>& self)
{
    const auto prescalerRaw = static_cast<double>(self->table.pwmPrescaler);
    const auto maxCountRaw = static_cast<double>(self->table.pwmMaxCount);
    return static_cast<double>(72e6 / ((prescalerRaw + 1) * (maxCountRaw + 1)));
}

int dacEncoderRead(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber)
{
    uint16_t count;
    EncoderMode mode;
    int sign;
    switch (encoderNumber) {
        case 1:
            sign = 1 - 2 * static_cast<int>(self->table.gpioInputState.enc1Sign);
            mode = static_cast<EncoderMode>(self->table.enc1Config.mode);
            count = self->table.enc1Data;
            break;
        case 2:
            sign = 1 - 2 * static_cast<int>(self->table.gpioInputState.enc2Sign);
            mode = static_cast<EncoderMode>(self->table.enc2Config.mode);
            count = self->table.enc2Data;
            break;
        case 3:
            sign = 1 - 2 * static_cast<int>(self->table.gpioInputState.enc3Sign);
            mode = static_cast<EncoderMode>(self->table.enc3Config.mode);
            count = self->table.enc3Data;
            break;
        default:
            count = 0;
            mode = PulseCount;
            break;
    }

    if (mode == PulseCount)
        return static_cast<int>(static_cast<int16_t>(count));
    else
        return 10 * sign * static_cast<int>(static_cast<uint16_t>(count));
}

void dacEncoderConfig(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber, EncoderMode mode, EncoderResolution resolution, EncoderDirection direction, unsigned int clockCycles)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    clockCycles = std::min(clockCycles, static_cast<unsigned int>(15));
    switch (encoderNumber) {
        case 1:
            self->table.enc1Config.mode = static_cast<uint8_t>(mode);
            self->table.enc1Config.resolution = static_cast<uint8_t>(resolution);
            self->table.enc1Config.direction = static_cast<uint8_t>(direction);
            self->table.enc1Config.filter = clockCycles;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC1_CONFIG);
            break;

        case 2:
            self->table.enc2Config.mode = static_cast<uint8_t>(mode);
            self->table.enc2Config.resolution = static_cast<uint8_t>(resolution);
            self->table.enc2Config.direction = static_cast<uint8_t>(direction);
            self->table.enc2Config.filter = clockCycles;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC2_CONFIG);
            break;

        case 3:
            self->table.enc3Config.mode = static_cast<uint8_t>(mode);
            self->table.enc3Config.resolution = static_cast<uint8_t>(resolution);
            self->table.enc3Config.direction = static_cast<uint8_t>(direction);
            self->table.enc3Config.filter = clockCycles;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC3_CONFIG);
            break;

        default:
            break;
    }

    if (self->autoSync) dacSync(self);
}

void dacEncoderReset(const std::shared_ptr<DacImpl>& self, unsigned int encoderNumber)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    switch (encoderNumber) {
        case 1:
            self->table.enc1Data = 0;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC1_DATA);
            if (self->autoSync) dacSync(self);
            break;
        case 2:
            self->table.enc2Data = 0;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC2_DATA);
            if (self->autoSync) dacSync(self);
            break;
        case 3:
            self->table.enc3Data = 0;
            prvAddToSyncList(self, TABLE_ADDRESS_OF_ENC3_DATA);
            if (self->autoSync) dacSync(self);
            break;
        default:
            break;
    }
}

void dacEnableAutoRead(const std::shared_ptr<DacImpl>& self, double periodSecs, std::function<void(void)> function)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    // Set the user callback
    self->callbackUser = std::move(function);

    // and enable the auto read
    periodSecs = std::min(periodSecs, 2000.0);
    periodSecs = std::max(periodSecs, 0.123);
    self->table.autoReadPeriod = static_cast<uint16_t>((1.0f / periodSecs) / 125e-6);
    prvAddToSyncList(self, TABLE_ADDRESS_OF_AUTOREAD_PERIOD);
    if (self->autoSync) dacSync(self);
}

void dacDisableAutoRead(const std::shared_ptr<DacImpl>& self)
{
    const std::lock_guard<std::mutex> lock(*self->mutex);

    self->table.autoReadPeriod = 0;
    prvAddToSyncList(self, TABLE_ADDRESS_OF_AUTOREAD_PERIOD);
    if (self->autoSync) dacSync(self);
}

int dacIsAutoReadEnabled(const std::shared_ptr<DacImpl>& self)
{
    return static_cast<int>(self->table.autoReadPeriod > 0);
}

void DacAnalogOutWrite(const std::shared_ptr<DacImpl>& self, Channel channel, unsigned int value)
{
    switch (channel) {
        case Channel::CH1:
            self->table.dac1Data = value;
            break;
        case Channel::CH2:
            self->table.dac2Data = value;
            break;
        case Channel::CH3:
            self->table.dac3Data = value;
            break;
        case Channel::CH4:
            self->table.dac4Data = value;
            break;
        default:
            break;
    }
}

int dacGetFirmwareVersion(const std::shared_ptr<DacImpl>& self)
{
    return self->table.version;
}

int dacGetBoardId(const std::shared_ptr<DacImpl>& self)
{
    return self->table.boardId;
}

std::shared_ptr<DacImpl> dacInit(const std::string& boardAddress, bool autoSync, unsigned int timeoutMs, unsigned int maxRetries)
{
    // Reserve memory to the DacImpl object
    std::shared_ptr<DacImpl> self = std::make_shared<DacImpl>();

    // Initialize the asio objects
    self->asioContext = new asio::io_context();
    self->serialPort = new asio::serial_port(*self->asioContext);
    self->mutex = new std::mutex();

    // Initialize the asio thread
    self->connected = true;
    self->threadAsio = std::thread(prvThreadRun, self);

    // Initialize or reserve memory the other variables
    memset(&self->table, 0, sizeof(MemoryTable));
    commReceiverInit(&self->receiver);
    self->waitingRx = false;
    self->waitingTx = false;

    // Board configuration
    self->timeoutMs = timeoutMs;
    self->maxRetries = maxRetries;
    self->autoSync = autoSync;

#ifdef WIN32
    // Fix Windows problem with low resolution timers, see
    // https://randomascii.wordpress.com/2020/10/04/windows-timer-resolution-the-great-rule-change
    // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FTime%2FNtSetTimerResolution.html
    // http://undocumented.ntinternals.net/index.html?page=UserMode%2FUndocumented%20Functions%2FTime%2FNtQueryTimerResolution.html
    // https://learn.microsoft.com/en-us/windows/win32/api/timeapi/nf-timeapi-timebeginperiod
    // https://stackoverflow.com/questions/3141556/how-to-setup-timer-resolution-to-0-5-ms
    // https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-exsettimerresolution
    ULONG MinRes, MaxRes;
    NtQueryTimerResolution(&MinRes, &MaxRes, &self->oldResolution);
    if (self->oldResolution != MinRes)
    {
        NtSetTimerResolution(MaxRes, TRUE, &self->oldResolution);
    }
#endif

    // Configure the serial port
    self->serialPort->open(boardAddress);
    if (self->serialPort->is_open())
    {
#ifdef WIN32
        // In Microsoft Windows we need to add a few more options to decrease the latency
        // documentation available at:
        // https://docs.microsoft.com/pt-br/windows/win32/api/winbase/ns-winbase-commtimeouts?redirectedfrom=MSDN#remarks
        // and
        // https://stackoverflow.com/questions/10718693/set-low-latency-flag-on-a-serial-port-on-windows-in-c
        // with code example at:
        // https://valelab4.ucsf.edu/svn/micromanager2/trunk/DeviceAdapters/SerialManager/SerialManager.cpp
        auto portHandle = self->serialPort->native_handle();
        COMMTIMEOUTS timeouts;
        memset(&timeouts, 0, sizeof(timeouts));
        timeouts.ReadIntervalTimeout = MAXDWORD;
        timeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
        timeouts.WriteTotalTimeoutConstant = 1;
        //timeouts.ReadIntervalTimeout = 1;
        SetCommTimeouts(portHandle, &timeouts);
#endif

        // Make sure the board configuration is properly loaded into memory table buffer
        uint8_t i = maxRetries;
        while ((i > 0) && (self->table.boardId != 0x1D))
        {
            dacReadAll(self);
            delayMs(10);
            i = i - 1;
        }

        self->connected = (self->table.boardId == 0x1D);
    }

    // Mark the time that the configuration is finished
    self->t0 = getTimeMs();

    return self;
}

void dacDestructor(const std::shared_ptr<DacImpl>& self)
{
#if WIN32
    ULONG MinRes, MaxRes, CurrentRes;
    NtQueryTimerResolution(&MinRes, &MaxRes, &CurrentRes);
    if (self->oldResolution != CurrentRes)
    {
        NtSetTimerResolution(self->oldResolution, TRUE, &CurrentRes);
    }
#endif

    // good example using asio and serial port:
    // https://gist.github.com/yoggy/3323808
    //
    // adding boost to cmake:
    // https://stackoverflow.com/questions/54581717/adding-boost-to-cmake-project

    if (dacIsAutoReadEnabled(self))
    {
        dacDisableAutoRead(self);
        if (!self->autoSync)
        {
            dacSync(self);
        }
    }

    self->connected = false;
    if (self->serialPort->is_open())
    {
        self->serialPort->cancel();
        self->serialPort->close();
    }

    /*
     * Stop asio context (and wait for it).
     */
    self->asioContext->stop();
    while (!self->asioContext->stopped()) {}
    self->threadAsio.join();

//    delete self->serialPort;
//    delete self->asioContext;
//    delete self->mutex;
}


#ifdef WITH_NRFUSB

DacNrf::DacNrf(const std::string& portName, bool setAutoSync)
{
    begin(setAutoSync);

    // Increase the timeout value
    timeoutMs = 100;

    // Create an instance to the NrfUsb
    radio = new Nrfusb(
            (char*) portName.c_str(),
            /*
             * https://en.cppreference.com/w/cpp/utility/functional/bind
             * The argument "this" is a bound parameter
             * The other arguments are unbound parameters
             */
            std::bind(
                    &DacNrf::callbackWhatHappened,
                    this,
                    std::placeholders::_1,  // tx_ok
                    std::placeholders::_2,  // tx_fail
                    std::placeholders::_3   // rx_ready
                )
            );

    // Load the default radio configuration
    table[AddressOfSetupRadioChannel] = 113;
    table[AddressOfSetupRadioBoardAddress + 0] = 'R';
    table[AddressOfSetupRadioBoardAddress + 1] = 'X';
    table[AddressOfSetupRadioBoardAddress + 2] = 'a';
    table[AddressOfSetupRadioBoardAddress + 3] = 'a';
    table[AddressOfSetupRadioBoardAddress + 4] = 'a';
    table[AddressOfSetupRadioPcAddress + 0] = 'T';
    table[AddressOfSetupRadioPcAddress + 1] = 'X';
    table[AddressOfSetupRadioPcAddress + 2] = 'a';
    table[AddressOfSetupRadioPcAddress + 3] = 'a';
    table[AddressOfSetupRadioPcAddress + 4] = 'a';
}

DacNrf::~DacNrf()
{
    // good example using asio and serial port:
    // https://gist.github.com/yoggy/3323808
    //
    // adding boost to cmake:
    // https://stackoverflow.com/questions/54581717/adding-boost-to-cmake-project

    if (isAutoReadEnabled())
    {
        disableAutoRead();
        if (!autoSync) sync();
    }

    connected = false;

    radio->close();

    // let's wait some time until the thread ends itself
    delayMs(50);

    delete radio;
}

void DacNrf::connect()
{
    unsigned int i;

    // Was the radio found?
    isRadioConnected = radio->isChipConnected();

    // Configure the radio
    if (isRadioConnected)
    {
        radio->maskIRQ(true, true, false);
        radio->setChannel(113);
        radio->setDataRate(RF24_2MBPS);
        radio->openWritingPipe(&table[AddressOfSetupRadioBoardAddress]);
        radio->openReadingPipe(1, &table[AddressOfSetupRadioPcAddress]);
        radio->setRetries(3, 5);
        radio->startListening();

        // Try to read the board
        i = maxRetries;
        while ((i > 0) && (table[AddressOfSetupBoardId] != 0x1D))
        {
            readDataFromBoard(AddressOfActionPwmDutyCh1, 17);
            readDataFromBoard(AddressOfSetupAutoReadPeriodMs, 22);
            readDataFromBoard(AddressOfSetupRadioBoardAddress, 22);
            readDataFromBoard(AddressOfSetupEthernetMac, 20);
            delayMs(10);
            i = i - 1;
        }

        connected = (table[AddressOfSetupBoardId] == 0x1D);
    }
}

void DacNrf::setChannel(int channel)
{
    channel = std::min(channel, 125);
    table[AddressOfSetupRadioChannel] = channel;

    if (isRadioConnected)
    {
        radio->stopListening();
        radio->setChannel(table[AddressOfSetupRadioChannel]);
        radio->startListening();
    }
}

void DacNrf::setPowerLevel(rf24_pa_dbm_e level)
{
    bitWriteRw(&table[AddressOfSetupRadioConfig], static_cast<uint8_t>(level), 4, 3);

    if (isRadioConnected)
    {
        radio->stopListening();
        radio->setPALevel(static_cast<rf24_pa_dbm_e>(bitRead(&table[AddressOfSetupRadioConfig], 4, 3)));
        radio->startListening();
    }
}

void DacNrf::implRead()
{
//    callbackFinishedReceiving();
}

void DacNrf::implWrite(uint8_t* buffer, uint8_t size)
{
    (void) size;
    radio->comboWrite(buffer, 32);
}

void DacNrf::callbackWhatHappened(bool tx_ok, bool tx_fail, bool rx_ready)
{
    (void) tx_ok;
    (void) tx_fail;
    uint8_t size = 32;
    std::error_code error;

    if (rx_ready)
    {
        radio->read(rxBuffer, size);
        callbackFinishedReceiving(error, size);
    }
}

#endif
