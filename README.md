# The documentation be found in [https://opendac.gitlab.io/dac/dac-driver/](https://opendac.gitlab.io/dac/dac-driver/).

## I want to use this driver with my project using CMake

For such purpose, just copy and paste the following CMake block of code:

```
ExternalProject_Add (opendac
    PREFIX            opendac
    GIT_REPOSITORY    https://gitlab.com/opendac/dac/dac-driver.git
    GIT_TAG           main
    CMAKE_ARGS        -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/libs
    UPDATE_DISCONNECTED True
)
ExternalProject_Get_Property(opendac SOURCE_DIR BINARY_DIR)
add_library(libdac STATIC IMPORTED)
set_property(TARGET libdac PROPERTY IMPORTED_LOCATION "${CMAKE_CURRENT_BINARY_DIR}/libs/lib/libopendac.lib")
link_libraries(libdac)
include_directories(${CMAKE_CURRENT_BINARY_DIR}/libs/include)
```

