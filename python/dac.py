import time
from dacPython import DacCpp, Channel, LOW, HIGH, GpioMode


CH1, CH2, CH3, CH4 = (Channel.CH1, Channel.CH2, Channel.CH3, Channel.CH4)
CH5, CH6, CH7 = (Channel.CH5, Channel.CH6, Channel.CH7)
GpioModeInput = GpioMode.GpioModeInput
GpioModeOutput = GpioMode.GpioModeOutput
PulseCount, TimeMeasurement = (0, 1)
LowResolution, HighResolution = (0, 1)
CCW, CW = (0, 1)


def time_ms():
    return 1e-6 * time.time_ns()


def time_us():
    return 1e-3 * time.time_ns()


class DacUsb:
    def __init__(self, serial_port_path, set_auto_sync=True):
        self.board = DacCpp(serial_port_path, set_auto_sync)

    def is_connected(self) -> bool:
        """
        Verifies if communication with the board is established.

        :return: True if the board is connected or False otherwise.
        """
        return self.board.isConnected()

    def read_all(self):
        """
        It reads all the memory of the board. This command should not be used for reading the measurements.

        :return: nothing.
        """
        self.board.readAll()

    def read_measurements(self):
        """
        It reads all the measurements made by the board.

        :return: nothing.
        """
        self.board.readMeasurements()

    def sync(self):
        """
        Forces to write data to the board. It is useful to configure several peripherals with only one communication
        transaction. This function DOES NOT read data from the board.
        :return: nothing.
        """
        self.board.sync()

    def gpio_config(self, channel: int, pin_mode: GpioMode):
        """
        This method is used to configure some gpio channel.
        :param channel: the GPIO channel. Example: CH3.
        :param pin_mode: the pin operation mode.
        :return: nothing.
        """
        self.board.gpioConfig(channel, pin_mode)

    def gpio_write(self, channel: int, state: int):
        """
        This method writes HIGH or LOW value to an output digital pin.

        :param channel: the OUTPUT GPIO channel. Example: CH3.
        :param state: HIGH or LOW.
        :return: nothing.
        """
        self.board.digitalWrite(channel, state)

    def gpio_read(self, channel: int) -> int:
        """
        Reads the logic state of a digital input pin.

        :param channel: the INPUT GPIO channel. Example: CH1.
        :return: HIGH or LOW.
        """
        return self.board.digitalRead(channel)

    def analog_read(self, channel: int) -> float:
        """
        Reads the analog value at some analog channel.

        :param channel: the ANALOG channel. Example: CH4.
        :return: the voltage (in V) on the analog pin.
        """
        return self.board.analogRead(channel)

    def pwm_set_dead_time(self, dead_time_in_ns: int):
        """
        This method is used to set the PWM dead time of all channels.
        :param dead_time_in_ns: the new dead time value in nanoseconds.
        :return:
        """
        self.board.pwmSetDeadTime(dead_time_in_ns)

    def pwm_config(self, channel: int, mode: int, fast_mode: bool, ch_polarity: int, chn_polarity: int,
                   ch_idle_state: int, chn_idle_state: int):
        """
        Configures a PWM channel.
        :param channel: the PWM channel. Example: CH2.
        :param mode: the PWM mode. It can be 1 or 2.
        :param fast_mode: enable fast mode?
        :param ch_polarity: the polarity of PWM CH output. It can be HIGH or LOW.
        :param chn_polarity: the polarity of PWM CHN output. It can be HIGH or LOW.
        :param ch_idle_state: the idle state of PWM CH output. It can be HIGH or LOW.
        :param chn_idle_state: the idle state of PWM CHN output. It can be HIGH or LOW.
        :return:
        """

    def pwm_set_duty(self, channel: int, duty: float):
        """
        This method sets the duty cycle of some PWM channel.

        :param channel: the PWM channel. Example: CH3.
        :param duty: a real number between 0 and 1.
        :return: nothing.
        """
        self.board.pwmSetDuty(channel, duty)

    def pwm_get_duty(self, channel: int) -> float:
        """
        Returns the duty cycle at some PWM channel.

        :param channel: the PWM channel. Example: CH4.
        :return: the duty cycle at the PWM channel.
        """
        return self.board.pwmGetDuty(channel)

    def pwm_set_frequency(self, freq_in_hz: float):
        """
        This method sets the PWM frequency.

        :param freq_in_hz: the frequency (in Hz) of the PWM signal.
        :return: nothing.
        """
        self.board.pwmSetFrequency(freq_in_hz)

    def pwm_get_frequency(self) -> float:
        """
        Returns the actual PWM frequency in Hz.

        :return: a real number representing the PWM frequency.
        """
        return self.board.pwmGetFrequency()

    def encoder_read(self, encoder_number: int) -> int:
        """
        This method reads the encoder.

        :param encoder_number: is the encoder number. It can 1, 2 or 3.
        :return: if the encoder is in pulse count mode, then it will return the encoder pulse count value. If the
                 encoder is in time measurement mode, then it will return time in micro-seconds.
        """
        return self.board.encoderRead(encoder_number)

    def encoder_config(self, encoder_number: int, mode: int, resolution: int, positive_direction: int, clock_cycles: int):
        """
        Sets the encoder mode.
        :param encoder_number:  is the encoder number. It can 1, 2 or 3.
        :param mode: it can be encoderModePulseCount or encoderModeTimeMeasurement.
        :param resolution: it can be LowResolution or HighResolution.
        :param positive_direction: it can be CW or CCW.
        :param clock_cycles: it is the number of cpu cycles that the logic state of the channel must be constant to be
                             recognized as stable. It is a number between (including) 0 and 15.
        :return: nothing.
        """
        self.board.encoderSetDirection(encoder_number, positive_direction)

    def encoder_reset_counting(self, encoder_number: int):
        """
        This method resets the encoder count value.

        :param encoder_number: is the encoder number. It can 1, 2 or 3.
        :return: nothing.
        """
        self.board.encoderReset(encoder_number)

    def enable_auto_read(self, period_seconds: float, callback):
        """
        It enables the auto read feature. It will make the board to send all measurements periodically.

        :param period_seconds: is the period (in seconds) between the updates.
        :param callback: is a function defined by the user that will be called when new data is read from the
                         microcontroller.
        :return: nothing.
        """
        self.board.enableAutoRead(period_seconds, callback)

    def disable_auto_read(self):
        """
        It disables the auto read feature.

        :return: nothing.
        """
        self.board.disableAutoRead()

    def is_auto_read_enabled(self) -> bool:
        """
        Returns the state of autoRead.
        :return: True if it is enabled and False otherwise.
        """
        return self.board.isAutoReadEnabled()

    def get_firmware_version(self) -> int:
        """
        This method reads the firmware version.

        :return: an integer representing the version.
        """
        return self.board.getFirmwareVersion()

    def get_board_id(self) -> int:
        """
        This method reads the board id.

        :return: it is expected to read the value 29.
        """
        return self.board.getBoardId()


if __name__ == '__main__':
    board = DacUsb('/dev/ttyACM0')

    try:
        print("Press Ctrl-C to terminate the test")

        while True:
            tstart = time_us()
            board.digital_write(CH3, LOW)
            tend = time_us()
            print('Elapsed time: %.2f' % (tend - tstart))

            time.sleep(100e-3)
            tstart = time_us()
            board.digital_write(CH3, HIGH)
            tend = time_us()
            print('Elapsed time: %.2f' % (tend - tstart))
            time.sleep(200e-3)
            tstart = time_us()
            board.digital_write(CH3, LOW)
            tend = time_us()
            print('Elapsed time: %.2f' % (tend - tstart))
            time.sleep(100e-3)
            tstart = time_us()
            board.digital_write(CH3, HIGH)
            tend = time_us()
            print('Elapsed time: %.2f' % (tend - tstart))
            time.sleep(600e-3)

    except KeyboardInterrupt:
        pass

    print('Closing the board communication')
