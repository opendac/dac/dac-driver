#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include "dac.hpp"

namespace py = pybind11;

PYBIND11_MODULE(dacUsbPython, m) {
    py::enum_<Channel>(m, "Channel")
            .value("CH1", CH1)
            .value("CH2", CH2)
            .value("CH3", CH3)
            .value("CH4", CH4)
            .value("CH5", CH5)
            .value("CH6", CH6)
            .value("CH7", CH7);

    py::enum_<GpioMode>(m, "GpioMode")
            .value("GpioModeInput", GpioModeInput)
            .value("GpioModeOutput", GpioModeOutput);

    m.attr("LOW") = LOW;
    m.attr("HIGH") = HIGH;

    py::class_<Dac> dac(m, "DacCpp");

    dac.def(py::init<std::string&, bool>());

    dac.def(
            "isConnected",
            &Dac::isConnected
    );

    dac.def(
            "readAll",
            &Dac::readAll
    );

    dac.def(
            "readMeasurements",
            &Dac::readMeasurements
    );

    dac.def(
            "sync",
            &Dac::sync
    );

    dac.def(
            "gpioConfig",
            &Dac::gpioConfig
    );

    dac.def(
            "gpioWrite",
            &Dac::gpioWrite
    );

    dac.def(
            "gpioRead",
            &Dac::gpioRead
    );

    dac.def(
            "analogRead",
            &Dac::analogRead
    );

    dac.def(
            "pwmSetDeadTime",
            &Dac::pwmSetDeadTime
    );

    dac.def(
            "pwmConfig",
            &Dac::pwmConfig
    );

    dac.def(
            "pwmSetDuty",
            &Dac::pwmSetDuty
    );

    dac.def(
            "pwmGetDuty",
            &Dac::pwmGetDuty
    );

    dac.def(
            "pwmSetFrequency",
            &Dac::pwmSetFrequency
    );

    dac.def(
            "pwmGetFrequency",
            &Dac::pwmGetFrequency
    );

    dac.def(
            "encoderRead",
            &Dac::encoderRead
    );

    dac.def(
            "encoderConfig",
            &Dac::encoderConfig
    );

    dac.def(
            "encoderReset",
            &Dac::encoderReset
    );

    // the call_guard may be necessary because of this discussion:
    // https://stackoverflow.com/questions/60410178/how-to-invoke-python-function-as-a-callback-inside-c-thread-using-pybind11
    // https://github.com/ikuokuo/start-pybind11/blob/master/docs/cpp_thread_callback_python_function.md
    // https://pybind11.readthedocs.io/en/stable/advanced/classes.html#overriding-virtuals
    // https://github.com/pybind/pybind11/issues/1087
    // https://bugs.python.org/issue20891
    // https://docs.python.org/2/c-api/init.html#non-python-created-threads
    // https://pybind11.readthedocs.io/en/stable/advanced/misc.html#global-interpreter-lock-gil
    dac.def(
            "enableAutoRead",
            &Dac::enableAutoRead,
            py::call_guard<py::gil_scoped_release>()
    );

    dac.def(
            "disableAutoRead",
            &Dac::disableAutoRead,
            py::call_guard<py::gil_scoped_release>()
    );

    dac.def(
            "isAutoReadEnabled",
            &Dac::isAutoReadEnabled
    );

    dac.def(
            "getFirmwareVersion",
            &Dac::getFirmwareVersion
    );

    dac.def(
            "getBoardId",
            &Dac::getBoardId
    );
}
