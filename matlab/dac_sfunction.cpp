/**
  ******************************************************************************
  * @file    daq_sfunction.cpp
  * @author  José Roberto Colombo Junior
  * @brief   An implementation of the DAQ driver to the Simulink interface.
  ******************************************************************************
  * @attention This is free software licensed under GPL.
  *
  * @note A good source of information about Simulink execution sequence:
  * https://www.mathworks.com/help/simulink/sfg/how-the-simulink-engine-interacts-with-c-s-functions.html
  *
  ******************************************************************************
  */

#include <iostream>
#include <cstring>
#include <cmath>

// S-Function information
#define S_FUNCTION_NAME  dacSfunction
#define S_FUNCTION_LEVEL 2
#include "simstruc.h"

// Application libraries
#include "dac.hpp"
#include "memory_table.h"

// Definitions
constexpr bool disableAutoSync = false;

typedef enum {
    inPortPwmCh1 = 0,
    inPortPwmCh2,
    inPortPwmCh3,
    inPortGpioCh1,
    inPortGpioCh2,
    inPortGpioCh3,
    inPortGpioCh4,
    inPortGpioCh5,
    inPortGpioCh6,
    inPortGpioCh7,
    numberOfInputPorts
} inputPort;

typedef enum {
    outPortAdCh1 = 0,
    outPortAdCh2,
    outPortAdCh3,
    outPortAdCh4,
    outPortEnc1,
    outPortEnc2,
    outPortEnc3,
    outPortGpioCh1,
    outPortGpioCh2,
    outPortGpioCh3,
    outPortGpioCh4,
    outPortGpioCh5,
    outPortGpioCh6,
    outPortGpioCh7,
    outPortRealTime,
    numberOfOutputPorts
} outputPort;

typedef enum {
    positionSerialPort = 0,
    positionSampleFreqHz,
    positionGpioCh1Mode,
    positionGpioCh1FinalState,
    positionGpioCh2Mode,
    positionGpioCh2FinalState,
    positionGpioCh3Mode,
    positionGpioCh3FinalState,
    positionGpioCh4Mode,
    positionGpioCh4FinalState,
    positionGpioCh5Mode,
    positionGpioCh5FinalState,
    positionGpioCh6Mode,
    positionGpioCh6FinalState,
    positionGpioCh7Mode,
    positionGpioCh7FinalState,
    positionPwmDeadTimeNs,
    positionPwmFrequencyHz,
    positionPwm1Mode,
    positionPwm1FastMode,
    positionPwm1ChPolarity,
    positionPwm1ChnPolarity,
    positionPwm1ChIdleState,
    positionPwm1ChnIdleState,
    positionPwm1FinalDuty,
    positionPwm2Mode,
    positionPwm2FastMode,
    positionPwm2ChPolarity,
    positionPwm2ChnPolarity,
    positionPwm2ChIdleState,
    positionPwm2ChnIdleState,
    positionPwm2FinalDuty,
    positionPwm3Mode,
    positionPwm3FastMode,
    positionPwm3ChPolarity,
    positionPwm3ChnPolarity,
    positionPwm3ChIdleState,
    positionPwm3ChnIdleState,
    positionPwm3FinalDuty,
    positionEnc1Mode,
    positionEnc1Quadrature,
    positionEnc1Direction,
    positionEnc1Filter,
    positionEnc1ResetCount,
    positionEnc2Mode,
    positionEnc2Quadrature,
    positionEnc2Direction,
    positionEnc2Filter,
    positionEnc2ResetCount,
    positionEnc3Mode,
    positionEnc3Quadrature,
    positionEnc3Direction,
    positionEnc3Filter,
    positionEnc3ResetCount,
    numberOfParameters
} parameterPosition;

typedef struct {
    // General config
    char serialPort[20];
    
    // Sample time
    double sampleFreqHz;
    uint64_t firstInstantTimeUs;

    // Gpio
    uint8_t gpioCh1Mode;
    uint8_t gpioCh2Mode;
    uint8_t gpioCh3Mode;
    uint8_t gpioCh4Mode;
    uint8_t gpioCh5Mode;
    uint8_t gpioCh6Mode;
    uint8_t gpioCh7Mode;
    uint8_t gpioCh1FinalState;
    uint8_t gpioCh2FinalState;
    uint8_t gpioCh3FinalState;
    uint8_t gpioCh4FinalState;
    uint8_t gpioCh5FinalState;
    uint8_t gpioCh6FinalState;
    uint8_t gpioCh7FinalState;

    // PWM
    double pwmDeadTimeNs;
    double pwmFrequencyHz;
    double pwm1FinalDuty;
    double pwm2FinalDuty;
    double pwm3FinalDuty;
    PwmConfig pwm1;
    PwmConfig pwm2;
    PwmConfig pwm3;

    // Encoders
    EncoderConfig enc1;
    EncoderConfig enc2;
    EncoderConfig enc3;
    bool enc1ResetCount;
    bool enc2ResetCount;
    bool enc3ResetCount;
} BoardConfig;

/*
 * TODO use std::bind to move waiting static bool variable into BoardConfig struct.
 */
volatile static bool waiting;
void callbackReceivedData()
{
    /*
     * This function will be called automatically by the C++ driver. This function will allow the Simulink to run.
     */
    waiting = false;
    (void) waiting;
}

/*
double escLengthToDuty(const BoardConfig* config, double length_us)
{
    // limit to esc1MinPulseLengthUs <= length_us <= esc1MaxPulseLengthUs
    length_us = fmax(length_us, config->esc1MinPulseLengthUs);
    length_us = fmin(length_us, config->esc1MaxPulseLengthUs);
    return config->pwmFrequencyHz * (length_us / 1000000.0f);
}
*/

/*
 * The sizes information is used by Simulink to determine the S-function
 * block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    // Auxiliary variables
    uint32_t i;

    // Set the expected number parameters
    ssSetNumSFcnParams(S, parameterPosition::numberOfParameters);

    // Verify parameter count
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    // Any parameter is not tunable because they are used to configure the board
    for (i = 0; i < parameterPosition::numberOfParameters; i++)
        ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    // Set the number of continuous and discrete states of the block
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 1);

    // Set the input ports and remove direct feed through
    if (!ssSetNumInputPorts(S, inputPort::numberOfInputPorts)) return;
    for (i = 0; i < inputPort::numberOfInputPorts; i++)
    {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 0);
    }

    // All the output ports have one dimension
    if (!ssSetNumOutputPorts(S, outputPort::numberOfOutputPorts)) return;
    for (i = 0; i < outputPort::numberOfOutputPorts; i++)
        ssSetOutputPortWidth(S, i, 1);

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    // Reserve two elements: the struct with parameter and the quark driver object
    ssSetNumPWork(S, 2);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetOptions(
        S, 
        SS_OPTION_EXCEPTION_FREE_CODE       // it doesn't use routines which can throw exceptions
        | SS_OPTION_DISCRETE_VALUED_OUTPUT
        | SS_OPTION_PLACE_ASAP              // typically used by devices connecting to hardware
    );
}

#define MDL_INITIALIZE_CONDITIONS
static void mdlInitializeConditions(SimStruct* S)
{
    real_T* x0 = ssGetRealDiscStates(S);
    int i;

    for (i = 0; i < ssGetNumDiscStates(S); i++)
        x0[i] = 0.0;
}

/*
 * This function is used to specify the sample time(s) for your
 * S-function. You must register the same number of sample times as
 * specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    const double sampleTimeSecs = 1.0f / mxGetScalar(ssGetSFcnParam(S, positionSampleFreqHz));
    ssSetSampleTime(S, 0, sampleTimeSecs);
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

// Change to #undef to remove function
#define MDL_START
static void mdlStart(SimStruct *S)
{
    // Reserve memory to the struct of configuration options
    auto config = new BoardConfig;
    ssGetPWork(S)[0] = reinterpret_cast<void*>(config);

    // Try to open the board
    strcpy(config->serialPort, mxArrayToString(ssGetSFcnParam(S, positionSerialPort)));
    config->sampleFreqHz = mxGetScalar(ssGetSFcnParam(S, positionSampleFreqHz));
    auto board = new Dac(config->serialPort, disableAutoSync);
    ssGetPWork(S)[1] = reinterpret_cast<void*>(board);

    if (board->isConnected())
    {
        if (board->getFirmwareVersion() == 2)
        {
            // Copy configuration parameters
            config->gpioCh1Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh1Mode)) - 1);
            config->gpioCh2Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh2Mode)) - 1);
            config->gpioCh3Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh3Mode)) - 1);
            config->gpioCh4Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh4Mode)) - 1);
            config->gpioCh5Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh5Mode)) - 1);
            config->gpioCh6Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh6Mode)) - 1);
            config->gpioCh7Mode          = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh7Mode)) - 1);
            config->gpioCh1FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh1FinalState)) - 1);
            config->gpioCh2FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh2FinalState)) - 1);
            config->gpioCh3FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh3FinalState)) - 1);
            config->gpioCh4FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh4FinalState)) - 1);
            config->gpioCh5FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh5FinalState)) - 1);
            config->gpioCh6FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh6FinalState)) - 1);
            config->gpioCh7FinalState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionGpioCh7FinalState)) - 1);
            config->pwmDeadTimeNs        = mxGetScalar(ssGetSFcnParam(S, positionPwmDeadTimeNs));
            config->pwmFrequencyHz       = mxGetScalar(ssGetSFcnParam(S, positionPwmFrequencyHz));
            config->pwm1.mode            = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1Mode)));
            config->pwm1.fastMode        = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1FastMode)) > 0);
            config->pwm1.chPolarity      = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1ChPolarity)) - 1);
            config->pwm1.chnPolarity     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1ChnPolarity)) - 1);
            config->pwm1.chIdleState     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1ChIdleState)) - 1);
            config->pwm1.chnIdleState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm1ChnIdleState)) - 1);
            config->pwm1FinalDuty        = mxGetScalar(ssGetSFcnParam(S, positionPwm1FinalDuty));
            config->pwm2.mode            = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2Mode)));
            config->pwm2.fastMode        = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2FastMode)) > 0);
            config->pwm2.chPolarity      = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2ChPolarity)) - 1);
            config->pwm2.chnPolarity     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2ChnPolarity)) - 1);
            config->pwm2.chIdleState     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2ChIdleState)) - 1);
            config->pwm2.chnIdleState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm2ChnIdleState)) - 1);
            config->pwm2FinalDuty        = mxGetScalar(ssGetSFcnParam(S, positionPwm2FinalDuty));
            config->pwm3.mode            = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3Mode)));
            config->pwm3.fastMode        = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3FastMode)) > 0);
            config->pwm3.chPolarity      = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3ChPolarity)) - 1);
            config->pwm3.chnPolarity     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3ChnPolarity)) - 1);
            config->pwm3.chIdleState     = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3ChIdleState)) - 1);
            config->pwm3.chnIdleState    = static_cast<uint8_t>(mxGetScalar(ssGetSFcnParam(S, positionPwm3ChnIdleState)) - 1);
            config->pwm3FinalDuty        = mxGetScalar(ssGetSFcnParam(S, positionPwm3FinalDuty));
            config->enc1.mode            = static_cast<EncoderMode>(mxGetScalar(ssGetSFcnParam(S, positionEnc1Mode)) - 1);
            config->enc1.direction       = static_cast<EncoderDirection>(mxGetScalar(ssGetSFcnParam(S, positionEnc1Direction)) - 1);
            config->enc1.resolution      = static_cast<EncoderResolution>(mxGetScalar(ssGetSFcnParam(S, positionEnc1Quadrature)) - 1);
            config->enc1.filter          = static_cast<unsigned int>(mxGetScalar(ssGetSFcnParam(S, positionEnc1Filter)));
            config->enc1ResetCount       = mxGetScalar(ssGetSFcnParam(S, positionEnc1ResetCount)) > 0;
            config->enc2.mode            = static_cast<EncoderMode>(mxGetScalar(ssGetSFcnParam(S, positionEnc2Mode)) - 1);
            config->enc2.direction       = static_cast<EncoderDirection>(mxGetScalar(ssGetSFcnParam(S, positionEnc2Direction)) - 1);
            config->enc2.resolution      = static_cast<EncoderResolution>(mxGetScalar(ssGetSFcnParam(S, positionEnc2Quadrature)) - 1);
            config->enc2.filter          = static_cast<unsigned int>(mxGetScalar(ssGetSFcnParam(S, positionEnc2Filter)));
            config->enc2ResetCount       = mxGetScalar(ssGetSFcnParam(S, positionEnc2ResetCount)) > 0;
            config->enc3.mode            = static_cast<EncoderMode>(mxGetScalar(ssGetSFcnParam(S, positionEnc3Mode)) - 1);
            config->enc3.direction       = static_cast<EncoderDirection>(mxGetScalar(ssGetSFcnParam(S, positionEnc3Direction)) - 1);
            config->enc3.resolution      = static_cast<EncoderResolution>(mxGetScalar(ssGetSFcnParam(S, positionEnc3Quadrature)) - 1);
            config->enc3.filter          = static_cast<unsigned int>(mxGetScalar(ssGetSFcnParam(S, positionEnc3Filter)));
            config->enc3ResetCount       = mxGetScalar(ssGetSFcnParam(S, positionEnc3ResetCount)) > 0;

            // Set the gpio configuration
            board->gpioConfig(CH1, static_cast<eGpioMode>(config->gpioCh1Mode));
            board->gpioConfig(CH2, static_cast<eGpioMode>(config->gpioCh2Mode));
            board->gpioConfig(CH3, static_cast<eGpioMode>(config->gpioCh3Mode));
            board->gpioConfig(CH4, static_cast<eGpioMode>(config->gpioCh4Mode));
            board->gpioConfig(CH5, static_cast<eGpioMode>(config->gpioCh5Mode));
            board->gpioConfig(CH6, static_cast<eGpioMode>(config->gpioCh6Mode));
            board->gpioConfig(CH7, static_cast<eGpioMode>(config->gpioCh7Mode));

            // Set the PWM configuration
            board->pwmSetFrequency(config->pwmFrequencyHz);
            board->pwmSetDeadTime(config->pwmDeadTimeNs);
            board->pwmConfig(
                    CH1,
                    config->pwm1.mode,
                    config->pwm1.fastMode,
                    config->pwm1.chPolarity,
                    config->pwm1.chnPolarity,
                    config->pwm1.chIdleState,
                    config->pwm1.chnIdleState
            );
            board->pwmConfig(
            CH2,
                    config->pwm2.mode,
                    config->pwm2.fastMode,
                    config->pwm2.chPolarity,
                    config->pwm2.chnPolarity,
                    config->pwm2.chIdleState,
                    config->pwm2.chnIdleState
            );
            board->pwmConfig(
            CH3,
                    config->pwm3.mode,
                    config->pwm3.fastMode,
                    config->pwm3.chPolarity,
                    config->pwm3.chnPolarity,
                    config->pwm3.chIdleState,
                    config->pwm3.chnIdleState
            );

            // Apply the encoder 1 configuration
            board->encoderConfig(
                    1,
                    static_cast<EncoderMode>(config->enc1.mode),
                    static_cast<EncoderResolution>(config->enc1.resolution),
                    static_cast<EncoderDirection>(config->enc1.direction),
                    config->enc1.filter
            );
            if (config->enc1ResetCount) board->encoderReset(1);

            // Apply the encoder 2 configuration
            board->encoderConfig(
                    2,
                    static_cast<EncoderMode>(config->enc2.mode),
                    static_cast<EncoderResolution>(config->enc2.resolution),
                    static_cast<EncoderDirection>(config->enc2.direction),
                    config->enc2.filter
            );
            if (config->enc2ResetCount) board->encoderReset(2);

            // Apply the encoder 3 configuration
            board->encoderConfig(
                    3,
                    static_cast<EncoderMode>(config->enc3.mode),
                    static_cast<EncoderResolution>(config->enc3.resolution),
                    static_cast<EncoderDirection>(config->enc3.direction),
                    config->enc3.filter
            );
            if (config->enc3ResetCount) board->encoderReset(3);

            // Enable the auto-read
            board->enableAutoRead(config->sampleFreqHz, callbackReceivedData);
            board->sync();
        }
        else
        {
            ssSetErrorStatus(S, "Firmware version is not compatible with this sFunction. It should be 2.");
        }
    }
    else
    {
        ssSetErrorStatus(S, "Could not connect to the board.");
    }
}

#define MDL_UPDATE
static void mdlUpdate(SimStruct* S, int_T tid)
{
    // Auxiliary variables
    UNUSED_ARG(tid);
    const InputRealPtrsType GpioCh1 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh1);
    const InputRealPtrsType GpioCh2 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh2);
    const InputRealPtrsType GpioCh3 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh3);
    const InputRealPtrsType GpioCh4 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh4);
    const InputRealPtrsType GpioCh5 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh5);
    const InputRealPtrsType GpioCh6 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh6);
    const InputRealPtrsType GpioCh7 = ssGetInputPortRealSignalPtrs(S, inPortGpioCh7);
    const InputRealPtrsType PwmCh1  = ssGetInputPortRealSignalPtrs(S, inPortPwmCh1);
    const InputRealPtrsType PwmCh2  = ssGetInputPortRealSignalPtrs(S, inPortPwmCh2);
    const InputRealPtrsType PwmCh3  = ssGetInputPortRealSignalPtrs(S, inPortPwmCh3);
    real_T* block_state             = ssGetRealDiscStates(S);

    // Load the board configuration and board driver
    auto config = reinterpret_cast<BoardConfig*>(ssGetPWork(S)[0]);
    auto board = reinterpret_cast<Dac*>(ssGetPWork(S)[1]);

    // Handle the gpio output
    board->gpioWrite(CH1, *GpioCh1[0] > 0);
    board->gpioWrite(CH2, *GpioCh2[0] > 0);
    board->gpioWrite(CH3, *GpioCh3[0] > 0);
    board->gpioWrite(CH4, *GpioCh4[0] > 0);
    board->gpioWrite(CH5, *GpioCh5[0] > 0);
    board->gpioWrite(CH6, *GpioCh6[0] > 0);
    board->gpioWrite(CH7, *GpioCh7[0] > 0);

    // Handle the pwm/esc
    board->pwmSetDuty(CH1, *PwmCh1[0]);
    board->pwmSetDuty(CH2, *PwmCh2[0]);
    board->pwmSetDuty(CH3, *PwmCh3[0]);

    // Sync the board
    board->sync();

    // Increase mdlUpdate count (used to force simulink to run this function)
    block_state[0] = block_state[0] + 1;

    // Wait until next sampling time
    waiting = true;
    const uint64_t timeoutUs = getTimeUs() + static_cast<uint64_t>((1.1e6 / config->sampleFreqHz));
    while ((waiting) && (getTimeUs() < timeoutUs)) {}
}

/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    // Auxiliary variables
    (void)tid;
    real_T* yGpioInCh1 = ssGetOutputPortRealSignal(S, outPortGpioCh1);
    real_T* yGpioInCh2 = ssGetOutputPortRealSignal(S, outPortGpioCh2);
    real_T* yGpioInCh3 = ssGetOutputPortRealSignal(S, outPortGpioCh3);
    real_T* yGpioInCh4 = ssGetOutputPortRealSignal(S, outPortGpioCh4);
    real_T* yGpioInCh5 = ssGetOutputPortRealSignal(S, outPortGpioCh5);
    real_T* yGpioInCh6 = ssGetOutputPortRealSignal(S, outPortGpioCh6);
    real_T* yGpioInCh7 = ssGetOutputPortRealSignal(S, outPortGpioCh7);
    real_T* yAdCh1 = ssGetOutputPortRealSignal(S, outPortAdCh1);
    real_T* yAdCh2 = ssGetOutputPortRealSignal(S, outPortAdCh2);
    real_T* yAdCh3 = ssGetOutputPortRealSignal(S, outPortAdCh3);
    real_T* yAdCh4 = ssGetOutputPortRealSignal(S, outPortAdCh4);
    real_T* yEnc1 = ssGetOutputPortRealSignal(S, outPortEnc1);
    real_T* yEnc2 = ssGetOutputPortRealSignal(S, outPortEnc2);
    real_T* yEnc3 = ssGetOutputPortRealSignal(S, outPortEnc3);
    real_T* yRealTime = ssGetOutputPortRealSignal(S, outPortRealTime);
    time_T simulation_time = ssGetT(S);

    // Load the board configuration and board driver
    auto config = reinterpret_cast<BoardConfig *>(ssGetPWork(S)[0]);
    auto board = reinterpret_cast<Dac*>(ssGetPWork(S)[1]);

    // Handle gpio
    *yGpioInCh1 = static_cast<real_T>(board->gpioRead(CH1));
    *yGpioInCh2 = static_cast<real_T>(board->gpioRead(CH2));
    *yGpioInCh3 = static_cast<real_T>(board->gpioRead(CH3));
    *yGpioInCh4 = static_cast<real_T>(board->gpioRead(CH4));
    *yGpioInCh5 = static_cast<real_T>(board->gpioRead(CH5));
    *yGpioInCh6 = static_cast<real_T>(board->gpioRead(CH6));
    *yGpioInCh7 = static_cast<real_T>(board->gpioRead(CH7));

    // Handle encoders
    *yEnc1 = static_cast<real_T>(board->encoderRead(1));
    *yEnc2 = static_cast<real_T>(board->encoderRead(2));
    *yEnc3 = static_cast<real_T>(board->encoderRead(3));

    // Handle A/D
    *yAdCh1 = static_cast<real_T>(board->analogRead(CH1));
    *yAdCh2 = static_cast<real_T>(board->analogRead(CH2));
    *yAdCh3 = static_cast<real_T>(board->analogRead(CH3));
    *yAdCh4 = static_cast<real_T>(board->analogRead(CH4));

    // Initialize the first instant time
    if (simulation_time == 0)
    {
        config->firstInstantTimeUs = getTimeUs();
    }

    // Put this time instant in output port
    *yRealTime = 1e-6 * static_cast<double>(getTimeUs() - config->firstInstantTimeUs);
}

/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
    auto config = reinterpret_cast<BoardConfig*>(ssGetPWork(S)[0]);
    auto board = reinterpret_cast<Dac*>(ssGetPWork(S)[1]);

    // Apply the final gpio state
    board->gpioWrite(CH1, config->gpioCh1FinalState);
    board->gpioWrite(CH2, config->gpioCh2FinalState);
    board->gpioWrite(CH3, config->gpioCh3FinalState);
    board->gpioWrite(CH4, config->gpioCh4FinalState);
    board->gpioWrite(CH5, config->gpioCh5FinalState);
    board->gpioWrite(CH6, config->gpioCh6FinalState);
    board->gpioWrite(CH7, config->gpioCh7FinalState);

    // Apply the final duty cycle
    board->pwmSetDuty(CH1, config->pwm1FinalDuty);
    board->pwmSetDuty(CH2, config->pwm2FinalDuty);
    board->pwmSetDuty(CH3, config->pwm3FinalDuty);
    board->sync();

    while (board->isAutoReadEnabled())
    {
        board->disableAutoRead();
        board->sync();
        delayMs(10);
    }
    delete board;
    delete config;
}

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
